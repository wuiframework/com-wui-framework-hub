# com-wui-framework-hub v2019.1.0

> WUI Framework's synchronization hub.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

## History

### v2019.1.0
Usage of updater and reverse proxy for Hub deploy. Usage of typedefs.
### v2019.0.2
Usage of new app loader. Updated docker configuration.
### v2019.0.1
Enable to specify request patterns externally. Usage of extended default.confing. Updates in docker image.
### v2019.0.0
Added basic registry page. Added ability to check agents status. Integration of letencrypt, metrics and reverse proxy. 
Fixed compatibility with selfextractor. Fixed embedded mode for linux. Updated docker files.
### v2018.3.0
Added implementation based on REST services.
### v2018.2.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
 
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
