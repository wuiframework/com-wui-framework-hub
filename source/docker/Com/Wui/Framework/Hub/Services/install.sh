#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sourceFile="${BASH_SOURCE[0]}"
while [ -h "$sourceFile" ]; do
    sourceLink="$(readlink "$sourceFile")"
    if [[ $sourceLink == /* ]]; then
        sourceFile="$sourceLink"
    else
        sourceDir="$( dirname "$sourceFile" )"
        sourceFile="$sourceDir/$sourceLink"
    fi
done
binBase="$( cd -P "$( dirname "$sourceFile" )" && pwd )"

sudo docker-compose down

mkdir -p /var/wui/com-wui-framework-services/balancer
mkdir -p /var/wui/com-wui-framework-services/hub

cd /var/wui/com-wui-framework-services/balancer

curl -fsSL --compressed "https://hub.wuiframework.com/Update/com-wui-framework-localhost/Linux/app-linux-nodejs" > balancer.tar.gz
tar xf balancer.tar.gz
cd com-wui-framework-localhost-*
cp -r . ../
cd ..
rm -rf com-wui-framework-localhost-*
rm balancer.tar.gz

cp $binBase/WuiLocalhost.config.jsonp /var/wui/com-wui-framework-services/balancer/WuiLocalhost.config.jsonp

cd /var/wui/com-wui-framework-services/hub

curl -fsSL --compressed "https://hub.wuiframework.com/Update/com-wui-framework-hub/Linux/app-linux-nodejs" > hub.tar.gz
tar xf hub.tar.gz
cd com-wui-framework-hub-*
cp -r . ../
cd ..
rm -rf com-wui-framework-hub-*
rm hub.tar.gz

cp $binBase/WuiHub.config.jsonp /var/wui/com-wui-framework-services/hub/WuiHub.config.jsonp

cd ..
sudo docker-compose up -d
sudo docker exec -d wui-services /var/wui/com-wui-framework-services/hub/WuiHub start
sleep 1
sudo docker exec -d wui-services /var/wui/com-wui-framework-services/balancer/WuiLocalhost start
sleep 1
sudo docker exec -d wui-hub /var/wui/com-wui-framework-updater/WuiConnector agent
