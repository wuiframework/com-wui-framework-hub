/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;
    import CertsManager = Com.Wui.Framework.Localhost.Utils.CertsManager;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class CertsHandler extends BaseConnector {

        @Extern()
        public getCertsFor($domain : string, $callback : IResponse) : void {
            $callback = ResponseFactory.getResponse($callback);
            if (Loader.getInstance().getAppConfiguration().certs.whitelist.Contains($domain)) {
                new CertsManager($domain).Create(($status : boolean, $cert : string, $key : string) : void => {
                    if ($status) {
                        $callback.Send($cert, $key);
                    } else {
                        $callback.Send("", "");
                    }
                });
            } else {
                $callback.Send("", "");
            }
        }
    }
}
