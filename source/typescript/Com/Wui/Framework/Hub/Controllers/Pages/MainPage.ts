/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Controllers.Pages {
    "use strict";
    import BaseHttpResolver = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.BaseHttpResolver;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class MainPage extends BaseHttpResolver {

        protected resolver() : void {
            const EOL : string = StringUtils.NewLine(false);
            StaticPageContentManager.Clear();
            StaticPageContentManager.Title("WUI Framework Hub");
            StaticPageContentManager.FaviconSource("/resource/graphics/icon.ico");
            StaticPageContentManager.License(
                "<!--" + EOL +
                EOL +
                "Copyright (c) 2017-2019 NXP" + EOL +
                EOL +
                "SPDX-License-Identifier: BSD-3-Clause" + EOL +
                "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution" + EOL +
                "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText" + EOL +
                EOL +
                "-->"
            );
            if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
                let packageName : string = this.getEnvironmentArgs().getProjectName() + "-" + this.getEnvironmentArgs().getProjectVersion();
                packageName = StringUtils.Replace(packageName, ".", "-");
                StaticPageContentManager.HeadScriptAppend("resource/javascript/" + packageName + ".min.js");
                StaticPageContentManager.HeadScriptAppend("resource/javascript/loader.min.js");
            } else {
                StaticPageContentManager.BodyAppend("" +
                    "<div class=\"" + StringUtils.Remove(this.getNamespaceName(), ".") + "\">" + EOL +
                    "   <div class=\"" + this.getClassNameWithoutNamespace() + "\">" + EOL +
                    "       <div class=\"Logo\"></div>" + EOL +
                    "       <div class=\"Note\">" + EOL +
                    "Hub version: " + this.getEnvironmentArgs().getProjectVersion() + ", " +
                    "build: " + this.getEnvironmentArgs().getBuildTime() + EOL +
                    "       </div>" + EOL +
                    "   </div>" + EOL +
                    "</div>");
            }
            StaticPageContentManager.Draw();
        }
    }
}
