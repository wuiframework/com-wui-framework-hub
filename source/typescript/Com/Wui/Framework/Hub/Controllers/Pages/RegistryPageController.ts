/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Controllers.Pages {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import RegistryPageViewer = Com.Wui.Framework.Hub.Gui.BaseInterface.Viewers.Pages.RegistryPageViewer;
    import RegistryPage = Com.Wui.Framework.Hub.Gui.Pages.RegistryPage;
    import RegistryPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.RegistryPageViewerArgs;
    import RegistryPageDAO = Com.Wui.Framework.Hub.DAO.Pages.RegistryPageDAO;
    import IAgentInfo = Com.Wui.Framework.Services.Connectors.IAgentInfo;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ILiveContentFormatterPromise = Com.Wui.Framework.Services.Interfaces.ILiveContentFormatterPromise;
    import AgentsRegister = Com.Wui.Framework.Hub.Primitives.AgentsRegister;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import ConfigurationsManager = Com.Wui.Framework.Hub.Utils.ConfigurationsManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import SelfupdatesManager = Com.Wui.Framework.Hub.Utils.SelfupdatesManager;
    import FileUploadResolver = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileUploadResolver;
    import HttpManager = Com.Wui.Framework.Localhost.HttpProcessor.HttpManager;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import SelfupdatePackage = Com.Wui.Framework.Hub.Models.SelfupdatePackage;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class RegistryPageController extends Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController {
        private readonly client : IWebServiceClient;

        private static getContent($callback : IResponse) : void {
            const manager : HttpManager = Loader.getInstance().getHttpManager();
            const agents : IAgentInfo[] = AgentsRegister.getAgentsList();
            const configs : string[] = new ConfigurationsManager().getRegisterLinks();
            const updates : SelfupdatePackage[] = new SelfupdatesManager().getRegisterInfo();
            const cwd : string = Loader.getInstance().getProgramArgs().AppDataPath() +
                "/resource/data/" + StringUtils.Replace(FileUploadResolver.ClassName(), ".", "/");
            const files : string[] = [];
            Loader.getInstance().getFileSystemHandler().Expand(cwd + "/**/*").forEach(($path : string) : void => {
                if (!StringUtils.Contains($path, ".htaccess")) {
                    files.push(StringUtils.Replace($path, cwd, "/Download"));
                }
            });

            const createLinkElement : any = ($url : string) : string => {
                const link : string = manager.CreateLink($url);
                return "<a href=\"" + link + "\" target=\"_blank\">" + link + "</a>";
            };

            const createReport : any = ($header : string, $list : string[]) : string => {
                let content : string =
                    "<h3>" + $header + "</h3>" +
                    "<div class=\"Items\">";
                $list.forEach(($item : string) : void => {
                    content += createLinkElement($item) + StringUtils.NewLine();
                });
                return content + "</div>";
            };

            let content : string = "<h3>Agents:</h3>";
            agents.forEach(($agent : IAgentInfo) : void => {
                content +=
                    "<div class=\"Items\">" +
                    "<b>" + $agent.name + " v" + $agent.version + "</b>" + StringUtils.NewLine() +
                    "Server: " + $agent.domain + " [" + $agent.platform + "]" + StringUtils.NewLine() +
                    "Uptime: " + Convert.TimeToLongString(new Date().getTime() - new Date($agent.startTime).getTime()) +
                    " from " + $agent.startTime +
                    "</div>";
            });
            content += createReport("Configs:", configs);

            content += "<h3>Updates:</h3>";
            updates.forEach(($package : SelfupdatePackage) : void => {
                let buildTime : any = $package.buildTime;
                if (ObjectValidator.IsString(buildTime)) {
                    buildTime = StringUtils.ToInteger(buildTime);
                }
                const type : any = ObjectValidator.IsEmptyOrNull($package.type) ? "zip" : $package.type;
                content +=
                    "<div class=\"Items\">" +
                    createLinkElement($package.link) + " [" + Convert.TimeToGMTformat(buildTime) + ", " + type + "]" +
                    "</div>";
            });

            content += createReport("Downloads:", files);

            ResponseFactory.getResponse($callback).Send(content);
        }

        constructor() {
            super();

            this.setPageTitle("WUI Framework Registry");
            this.setDao(new RegistryPageDAO());
            this.setModelClassName(RegistryPageViewer);
            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getHttpManager().CreateLink("/connector.config.jsonp"));
        }

        protected onSuccess($instance? : RegistryPage, $args? : RegistryPageViewerArgs, $dao? : RegistryPageDAO) : void {
            this.getContent().Then(($content : string) : void => {
                $instance.content.Value($content);
            });
        }

        protected invoke($name : string, ...$args : string[]) : ILiveContentFormatterPromise {
            return LiveContentWrapper.InvokeMethod.apply(this, [this.client, this.getClassName(), $name].concat($args));
        }

        private getContent() : IRegisterPromise {
            return this.invoke("getContent");
        }
    }

    export interface IRegisterPromise {
        Then($callback : ($content : string) => void) : void;
    }
}
