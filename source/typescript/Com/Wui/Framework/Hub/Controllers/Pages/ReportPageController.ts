/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Controllers.Pages {
    "use strict";
    import ReportAPI = Com.Wui.Framework.Hub.Utils.ReportAPI;
    import ReportPageViewer = Com.Wui.Framework.Hub.Gui.BaseInterface.Viewers.Pages.ReportPageViewer;
    import ReportPage = Com.Wui.Framework.Hub.Gui.Pages.ReportPage;
    import ReportPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.ReportPageViewerArgs;
    import ReportPageDAO = Com.Wui.Framework.Hub.DAO.Pages.ReportPageDAO;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IReportsPromise = Com.Wui.Framework.Hub.Utils.IReportsPromise;
    import IReportContentPromise = Com.Wui.Framework.Hub.Utils.IReportContentPromise;

    export class ReportPageController extends RegistryPageController {
        private appId : string;

        private static getReports($appId : string, $callback : IResponse) : void {
            ResponseFactory.getResponse($callback).Send(new ReportAPI().getReports($appId));
        }

        private static getReport($id : string, $callback : IResponse) : void {
            ResponseFactory.getResponse($callback).Send(new ReportAPI().getContent($id));
        }

        constructor() {
            super();

            this.setPageTitle("WUI Framework Report");
            this.setDao(new ReportPageDAO());
            this.setModelClassName(ReportPageViewer);
            this.appId = "";
        }

        protected argsHandler($GET : ArrayList<string>) : void {
            this.appId = $GET.getItem("appId");
        }

        protected onSuccess($instance? : ReportPage, $args? : ReportPageViewerArgs, $dao? : ReportPageDAO) : void {
            this.getReports(this.appId).Then(($list : string[]) : void => {
                $list.forEach(($item : string) : void => {
                    $instance.reports.Add(StringUtils.Substring($item, StringUtils.IndexOf($item, "/", false) + 1), $item);
                });
            });

            $instance.reports.getEvents().setOnSelect(() : void => {
                this.getReport($instance.reports.Value()).Then(($content : string) : void => {
                    if (StringUtils.EndsWith($instance.reports.Value(), ".png")) {
                        $content = "<img src=\"" + $content + "\">";
                    } else {
                        $content = StringUtils.Replace($content, "INFO:", "\nINFO:");
                        $content = StringUtils.Replace($content, "DEBUG:", "\nDEBUG:");
                        $content = StringUtils.Replace($content, "ERROR:", "\nERROR:");
                        $content = StringUtils.Replace($content, "WARNING:", "\nWARNING:");
                        let index : number = 0;
                        const length : number = StringUtils.Length($content);
                        let wrapped : string = "";
                        let lastBreak : number = 0;
                        while (index < length) {
                            if ($content[index] === "\n") {
                                lastBreak = index;
                            }
                            if (index - lastBreak > 140) {
                                wrapped += "\n";
                                lastBreak = index;
                            }
                            wrapped += $content[index];
                            index++;
                        }
                        wrapped = StringUtils.Replace(wrapped, "ERROR:", "<span class=\"Error\">ERROR:</span>");
                        wrapped = StringUtils.Replace(wrapped, "WARNING:", "<span class=\"Warning\">WARNING:</span>");
                        $content = StringUtils.Replace(wrapped, "\n", StringUtils.NewLine());
                    }
                    $instance.content.Value($content);
                });
            });
        }

        private getReports($appId : string) : IReportsPromise {
            return this.invoke("getReports", $appId);
        }

        private getReport($id : string) : IReportContentPromise {
            return this.invoke("getReport", $id);
        }
    }
}
