/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.DAO.Pages {
    "use strict";
    import BaseDAO = Com.Wui.Framework.Services.DAO.BaseDAO;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class BasePageDAO extends Com.Wui.Framework.Services.DAO.BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Com/Wui/Framework/Hub/Localization/BasePageLocalization.jsonp";

        protected static getDaoInterfaceClassName($interfaceName : string) : any {
            switch ($interfaceName) {
            case "IBasePageConfiguration":
                return Pages.BasePageDAO;
            case "IReportPageLocalization":
                return Pages.ReportPageDAO;
            case "IRegistryPageLocalization":
                return Pages.RegistryPageDAO;
            default:
                break;
            }
            return BaseDAO;
        }
    }
}
