/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import RegistryPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.RegistryPageViewerArgs;
    import IRegistryPageLocalization = Com.Wui.Framework.Hub.Interfaces.DAO.IRegistryPageLocalization;

    export class RegistryPageDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Com/Wui/Framework/Hub/Localization/RegistryPageLocalization.jsonp";

        public getPageConfiguration() : IRegistryPageLocalization {
            return <IRegistryPageLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : RegistryPageViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const pageConfiguration : IRegistryPageLocalization = this.getPageConfiguration();
                const args : RegistryPageViewerArgs = new RegistryPageViewerArgs();
                args.HeaderText(pageConfiguration.header);
                args.FooterText(pageConfiguration.footer);

                this.modelArgs = args;
            }
            return <RegistryPageViewerArgs>this.modelArgs;
        }
    }
}
