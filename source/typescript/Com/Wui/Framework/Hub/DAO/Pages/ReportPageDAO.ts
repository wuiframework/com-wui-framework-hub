/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IReportPageLocalization = Com.Wui.Framework.Hub.Interfaces.DAO.IReportPageLocalization;
    import ReportPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.ReportPageViewerArgs;

    export class ReportPageDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Com/Wui/Framework/Hub/Localization/ReportPageLocalization.jsonp";

        public getPageConfiguration() : IReportPageLocalization {
            return <IReportPageLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : ReportPageViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const pageConfiguration : IReportPageLocalization = this.getPageConfiguration();
                const args : ReportPageViewerArgs = new ReportPageViewerArgs();
                args.HeaderText(pageConfiguration.header);
                args.ReportsHint(pageConfiguration.reportsHint);
                args.FooterText(pageConfiguration.footer);

                this.modelArgs = args;
            }
            return <ReportPageViewerArgs>this.modelArgs;
        }
    }
}
