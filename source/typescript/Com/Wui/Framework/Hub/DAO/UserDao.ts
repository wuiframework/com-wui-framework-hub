/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.DAO {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import User = Com.Wui.Framework.Hub.Models.User;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;

    export class UserDao extends BaseObject {
        private readonly id : string;
        private data : User;

        constructor($id : string) {
            super();
            this.id = $id;
        }

        public Save() : boolean {
            if (!ObjectValidator.IsEmptyOrNull(this.data)) {
                this.data.Id(this.id);
                return Loader.getInstance().getFileSystemHandler().Write(this.getDatabasePath(), ObjectEncoder.Serialize(this.data));
            }
            return false;
        }

        public getModel() : User {
            if (ObjectValidator.IsEmptyOrNull(this.data)) {
                const filepath : string = this.getDatabasePath();
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                if (!ObjectValidator.IsEmptyOrNull(this.id) && fileSystem.Exists(filepath)) {
                    this.data = ObjectDecoder.Unserialize(<string>fileSystem.Read(filepath));
                } else {
                    this.data = new User();
                    this.data.Id(this.id);
                }
            }
            return this.data;
        }

        public Exists() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.id) && Loader.getInstance().getFileSystemHandler().Exists(this.getDatabasePath());
        }

        public Delete() : void {
            Loader.getInstance().getFileSystemHandler().Delete(this.getDatabasePath());
            this.data = null;
        }

        private getDatabasePath() : string {
            const path : string = Loader.getInstance().getProgramArgs().AppDataPath() +
                "/resource/data/" + StringUtils.Replace(this.getClassName(), ".", "/") + "/" + this.id;
            this.getDatabasePath = () : string => {
                return path;
            };
            return path;
        }
    }
}
