/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Gui.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ReportPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.ReportPageViewerArgs;
    import Label = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;

    export class RegistryPage extends Com.Wui.Framework.UserControls.Primitives.BasePanel {
        public header : Label;
        public content : Label;
        public footer : Label;

        constructor($id? : string) {
            super($id);

            this.header = new Label();
            this.content = new Label();
            this.footer = new Label();
        }

        public Value($value? : ReportPageViewerArgs) : ReportPageViewerArgs {
            return <ReportPageViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.content.getEvents().setOnChange(() : void => {
                RegistryPage.scrollBarVisibilityHandler(this);
            });

            return super.innerCode();
        }

        protected addContentItems($body : IGuiElement) : void {
            $body.Add(this.addRow().Add(this.content));
        }

        protected innerHtml() : IGuiElement {
            const html : IGuiElement = this.addColumn()
                .WidthOfColumn("100%", false)
                .FitToParent(FitToParent.FULL)
                .Alignment(Alignment.TOP_LEFT_PROPAGATED)
                .Add(this.addRow().HeightOfRow("80px").StyleClassName("Header")
                    .Add(this.addColumn().Add(this.header))
                    .Add(this.addColumn().WidthOfColumn("300px").StyleClassName("Logo")));
            this.addContentItems(html);
            html.Add(this.addRow()
                .Alignment(Alignment.CENTER_PROPAGATED)
                .HeightOfRow("60px")
                .StyleClassName("Footer")
                .Add(this.footer));
            return html;
        }
    }
}
