/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Gui.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ReportPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.ReportPageViewerArgs;
    import DropDownList = Com.Wui.Framework.UserControls.BaseInterface.UserControls.DropDownList;

    export class ReportPage extends RegistryPage {
        public reports : DropDownList;

        constructor($id? : string) {
            super($id);

            this.reports = new DropDownList();
        }

        public Value($value? : ReportPageViewerArgs) : ReportPageViewerArgs {
            return <ReportPageViewerArgs>super.Value($value);
        }

        protected innerCode() : IGuiElement {
            this.Scrollable(true);

            this.reports.MaxVisibleItemsCount(10);
            this.reports.ShowButton(false);
            this.content.StyleClassName("Log");
            this.content.getEvents().setOnChange(() : void => {
                ReportPage.scrollBarVisibilityHandler(this);
            });

            return super.innerCode();
        }

        protected addContentItems($body : IGuiElement) : void {
            $body.Add(this.addRow().HeightOfRow("40px").Add(this.reports));
            super.addContentItems($body);
        }
    }
}
