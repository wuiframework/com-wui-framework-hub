/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Gui.BaseInterface.Viewers.Pages {
    "use strict";
    import RegistryPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.RegistryPageViewerArgs;
    import RegistryPage = Com.Wui.Framework.Hub.Gui.Pages.RegistryPage;

    export class RegistryPageViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        /* dev:start */
        protected static getTestViewerArgs() : RegistryPageViewerArgs {
            const args : RegistryPageViewerArgs = new RegistryPageViewerArgs();
            args.HeaderText("Registry");
            args.DefaultContent("Loading data ...");
            args.FooterText("Copyright (c) 2018 NXP");

            return args;
        }

        /* dev:end */

        constructor($args? : RegistryPageViewerArgs) {
            super($args);
            this.setInstance(new RegistryPage());
        }

        public getInstance() : RegistryPage {
            return <RegistryPage>super.getInstance();
        }

        public ViewerArgs($args? : RegistryPageViewerArgs) : RegistryPageViewerArgs {
            return <RegistryPageViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : RegistryPage, $args : RegistryPageViewerArgs) : void {
            $instance.header.Text($args.HeaderText());
            $instance.content.Text($args.DefaultContent());
            $instance.footer.Text($args.FooterText());
        }

        /* dev:start */
        protected testImplementation($instance : RegistryPage) : string {
            $instance.StyleClassName("TestCss");
            $instance.Height(500);

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .RegistryPage {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
