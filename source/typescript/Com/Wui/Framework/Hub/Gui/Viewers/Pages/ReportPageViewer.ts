/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Gui.BaseInterface.Viewers.Pages {
    "use strict";
    import ReportPageViewerArgs = Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages.ReportPageViewerArgs;
    import ReportPage = Com.Wui.Framework.Hub.Gui.Pages.ReportPage;

    export class ReportPageViewer extends RegistryPageViewer {

        /* dev:start */
        protected static getTestViewerArgs() : ReportPageViewerArgs {
            const args : ReportPageViewerArgs = new ReportPageViewerArgs();
            args.HeaderText("Test");
            args.ReportsHint("Select report ...");
            args.FooterText("Copyright (c) 2018 NXP");

            return args;
        }

        /* dev:end */

        constructor($args? : ReportPageViewerArgs) {
            super($args);
            this.setInstance(new ReportPage());
        }

        public getInstance() : ReportPage {
            return <ReportPage>super.getInstance();
        }

        public ViewerArgs($args? : ReportPageViewerArgs) : ReportPageViewerArgs {
            return <ReportPageViewerArgs>super.ViewerArgs($args);
        }

        protected argsHandler($instance : ReportPage, $args : ReportPageViewerArgs) : void {
            super.argsHandler($instance, $args);
            $instance.reports.Hint($args.ReportsHint());
        }

        /* dev:start */
        protected testImplementation($instance : ReportPage) : string {
            $instance.StyleClassName("TestCss");
            $instance.Height(500);
            $instance.reports.Add("report 1");
            $instance.reports.Add("report 2");
            $instance.reports.Add("report 3");
            $instance.reports.Add("report 4");
            $instance.reports.getEvents().setOnSelect(() : void => {
                $instance.content.Value($instance.reports.Value());
            });

            return "<style>" +
                "body {overflow: hidden!important;} " +
                ".TestCss .ReportPage {border: 1px solid #E7E7E8;}</style>";
        }

        /* dev:end */
    }
}
