/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class RegistryPageViewerArgs extends Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs {
        private headerText : string;
        private defaultContent : string;
        private footerText : string;

        constructor() {
            super();
            this.headerText = "";
            this.defaultContent = "";
            this.footerText = "";
        }

        public HeaderText($value? : string) : string {
            return this.headerText = Property.String(this.headerText, $value);
        }

        public DefaultContent($value? : string) : string {
            return this.defaultContent = Property.String(this.defaultContent, $value);
        }

        public FooterText($value? : string) : string {
            return this.footerText = Property.String(this.footerText, $value);
        }
    }
}
