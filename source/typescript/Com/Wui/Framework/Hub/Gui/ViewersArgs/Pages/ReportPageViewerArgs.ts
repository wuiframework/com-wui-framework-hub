/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Gui.ViewersArgs.Pages {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class ReportPageViewerArgs extends RegistryPageViewerArgs {
        private reportsHint : string;

        constructor() {
            super();
            this.reportsHint = "";
        }

        public ReportsHint($value? : string) : string {
            return this.reportsHint = Property.String(this.reportsHint, $value);
        }
    }
}
