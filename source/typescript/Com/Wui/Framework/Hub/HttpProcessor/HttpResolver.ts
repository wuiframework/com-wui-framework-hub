/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor {
    "use strict";

    export class HttpResolver extends Com.Wui.Framework.Localhost.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            let indexClass : any = Com.Wui.Framework.Hub.Controllers.Pages.MainPage;
            if (!Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                indexClass = Com.Wui.Framework.Hub.Index;
            }
            resolvers["/"] = indexClass;
            resolvers["/index"] = indexClass;
            resolvers["/index.html"] = indexClass;
            resolvers["/web/"] = indexClass;
            resolvers["/report/{appId}"] = Com.Wui.Framework.Hub.Controllers.Pages.ReportPageController;
            resolvers["/registry"] = Com.Wui.Framework.Hub.Controllers.Pages.RegistryPageController;

            if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                return resolvers;
            }

            resolvers["/liveContent/"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.LiveContentResolver;
            resolvers["/ping.txt"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.PingResolver;
            resolvers["/ping.png"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.PingResolver;

            const fileResolver : any = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileRequestResolver;
            resolvers["/robots.txt"] = fileResolver;
            resolvers["/Download/{fileId}"] = fileResolver;
            resolvers["/Configuration/{appName}/{configName}"] = fileResolver;
            resolvers["/Configuration/{appName}/{configName}/{version}"] = fileResolver;
            resolvers["/Update/{appName}"] = fileResolver;
            resolvers["/Update/{appName}/{releaseName}"] = fileResolver;
            resolvers["/Update/{appName}/{releaseName}/{platform}"] = fileResolver;
            resolvers["/Update/{appName}/{releaseName}/{platform}/{version}"] = fileResolver;

            resolvers["/Upload"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileUploadResolver;
            resolvers["/Upload/{type}"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileUploadResolver;
            resolvers["/Upload/{type}/{clientId}"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileUploadResolver;
            resolvers["/DynamicImage/{type}/{source}/{settings}/{version}"] =
                Com.Wui.Framework.Hub.HttpProcessor.Resolvers.ImageRequestResolver;

            resolvers["/forward"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.ForwardingResolver;
            resolvers["/forward/continue"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.ForwardingResolver;

            resolvers["/Plugins/{ideName}"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.PluginsRepository;
            resolvers["/Plugins/{ideName}/{repoDescriptor}"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.PluginsRepository;
            resolvers["/Plugins/{ideName}/{repoDescriptor}/{pluginName}"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.PluginsRepository;

            resolvers["/Status/Agent/{name}"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.AgentStatusResolver;

            return resolvers;
        }
    }
}
