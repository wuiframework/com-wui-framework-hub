/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import IAgentInfo = Com.Wui.Framework.Services.Connectors.IAgentInfo;
    import AgentsRegister = Com.Wui.Framework.Hub.Primitives.AgentsRegister;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class AgentStatusResolver extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.BaseHttpResolver {
        private name : string;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            super.argsHandler($GET, $POST);
            this.name = null;
            if ($GET.KeyExists("name")) {
                this.name = ObjectDecoder.Url($GET.getItem("name"));
            }
        }

        protected resolver() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.name)) {
                const agents : IAgentInfo[] = AgentsRegister.getAgentsList();
                let agent : IAgentInfo = null;
                agents.forEach(($agent : IAgentInfo) : void => {
                    if ($agent.name === this.name) {
                        agent = $agent;
                    }
                });
                if (!ObjectValidator.IsEmptyOrNull(agent)) {
                    this.getConnector().Send({
                        body  : "Agent \"" + this.name + "\" uptime: " +
                            Convert.TimeToLongString(new Date().getTime() - new Date(agent.startTime).getTime()),
                        status: HttpStatusType.SUCCESS
                    });
                } else {
                    this.getConnector().Send({
                        body  : "Required agent \"" + this.name + "\" has not been found.",
                        status: HttpStatusType.NOT_FOUND
                    });
                }
            } else {
                this.getConnector().Send({
                    body  : "Agent name must be specified.",
                    status: HttpStatusType.ERROR
                });
            }
        }
    }
}
