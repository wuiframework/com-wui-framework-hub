/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IFilePath = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.IFilePath;
    import ConfigFile = Com.Wui.Framework.Hub.Models.ConfigFile;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ConfigurationsManager = Com.Wui.Framework.Hub.Utils.ConfigurationsManager;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import SelfupdatePackage = Com.Wui.Framework.Hub.Models.SelfupdatePackage;
    import SelfupdatesManager = Com.Wui.Framework.Hub.Utils.SelfupdatesManager;

    export class FileRequestResolver extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.FileRequestResolver {
        private config : ConfigFile;
        private package : SelfupdatePackage;
        private fileId : string;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            super.argsHandler($GET, $POST);
            this.config = null;
            this.package = null;
            this.fileId = null;
            if ($GET.KeyExists("appName")) {
                if (StringUtils.ContainsIgnoreCase(this.getRequest().getUrl(), "/Configuration/")) {
                    this.config = new ConfigFile();
                    this.config.name = $GET.getItem("appName");
                    this.config.configName = $GET.getItem("configName");
                    this.config.version = $GET.getItem("version");
                } else {
                    this.package = new SelfupdatePackage();
                    this.package.name = $GET.getItem("appName");
                    this.package.release = $GET.getItem("releaseName");
                    this.package.platform = $GET.getItem("platform");
                    this.package.version = $GET.getItem("version");
                }
            }
            if ($GET.KeyExists("fileId")) {
                this.fileId = ObjectDecoder.Base64($GET.getItem("fileId"));
            }
        }

        protected filePathResolver() : IFilePath {
            if (!ObjectValidator.IsEmptyOrNull(this.config)) {
                if (ObjectValidator.IsEmptyOrNull(this.config.version)) {
                    this.config.version = null;
                }
                const manager : ConfigurationsManager = ConfigurationsManager.getInstance();
                const packageInfo : IFilePath =
                    manager.Download(this.config.name, this.config.configName, this.config.version);
                packageInfo.url = this.getRequest().getUrl();
                return packageInfo;
            } else if (!ObjectValidator.IsEmptyOrNull(this.package)) {
                const manager : SelfupdatesManager = SelfupdatesManager.getInstance();
                const packageInfo : IFilePath =
                    manager.Download(this.package.name, this.package.release, this.package.platform, this.package.version);
                packageInfo.url = this.getRequest().getUrl();
                return packageInfo;
            } else if (StringUtils.ContainsIgnoreCase(this.getRequest().getUrl(), "/Download") &&
                !ObjectValidator.IsEmptyOrNull(this.fileId)) {
                const path : Types.NodeJS.path = require("path");
                const name = path.basename(this.fileId);
                let extension = name;
                extension = StringUtils.Substring(extension,
                    StringUtils.IndexOf(name, ".", false) + 1, StringUtils.Length(name));
                extension = StringUtils.ToLowerCase(extension);
                const filePath : string = path.normalize(Loader.getInstance().getProgramArgs().AppDataPath() +
                    "/resource/data/" + StringUtils.Replace(FileUploadResolver.ClassName(), ".", "/") + "/" + name);

                return {
                    extension,
                    name,
                    path: filePath,
                    url : this.getRequest().getUrl()
                };
            } else {
                return super.filePathResolver();
            }
        }

        protected getAllowedPatterns() : string[] {
            return super.getAllowedPatterns().concat([
                "/Configuration/*",
                "/Update/*",
                "/Download/*",
                "/robots.txt"
            ]);
        }

        protected resolver() : void {
            if (StringUtils.Contains(this.fileId, "http://", "https://")) {
                LogIt.Debug("init proxy stream for: {0}", this.fileId);
                const options : Types.NodeJS.Modules.url.UrlWithStringQuery = require("url").parse(this.fileId);
                const httpRequest : Types.NodeJS.Modules.http.ClientRequest =
                    (options.protocol === "https:" ? require("https") : require("http"))
                        .request(options, ($httpResponse : any) : void => {
                            $httpResponse.headers["content-encoding"] = "identity";
                            this.getConnector().Send({
                                body   : $httpResponse,
                                headers: $httpResponse.headers,
                                status : $httpResponse.statusCode
                            });
                        });
                httpRequest.on("error", ($ex : Error) : void => {
                    LogIt.Warning($ex.stack);
                });
                httpRequest.end();
            } else {
                super.resolver();
            }
        }
    }
}
