/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import PersistenceFactory = Com.Wui.Framework.Gui.PersistenceFactory;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import BaseHttpResolver = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.BaseHttpResolver;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IFileUploadProtocol = Com.Wui.Framework.Gui.Interfaces.Components.IFileUploadProtocol;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class FileUploadHandler extends Com.Wui.Framework.Localhost.Primitives.BaseConnector {

        @Extern()
        public static Upload($data : IFileUploadProtocol, $callback? : IResponse) : void {
            ResponseFactory.getResponse($callback).Send(this.getInstance<FileUploadHandler>().Upload($data));
        }

        public Upload($data : IFileUploadProtocol) : boolean {
            const name : string = ObjectDecoder.Base64($data.name);
            const path : string = Loader.getInstance().getProgramArgs().AppDataPath() +
                "/resource/data/" + StringUtils.Replace(FileUploadResolver.ClassName(), ".", "/") + "/" + $data.id;
            LogIt.Debug("Print file chunk {0} to {1} [{2}]", "" + $data.index, name, path);
            if (ObjectValidator.IsString($data.data)) {
                $data.data = <any>Buffer.from($data.data, "base64");
            }
            return Loader.getInstance().getFileSystemHandler().Write(path, $data.data, $data.index !== 0);
        }
    }

    export class FileUploadResolver extends BaseHttpResolver {
        private readonly uploadPath : string;
        private readonly files : any[];
        private fileIds : string[];
        private type : string;
        private clientId : string;
        private maxFileSize : number;

        public static getInstance() : FileUploadResolver {
            const instance : any = new this();
            this.getInstance = () : FileUploadResolver => {
                return instance;
            };
            return instance;
        }

        constructor() {
            super();
            this.uploadPath = Loader.getInstance().getProgramArgs().AppDataPath() +
                "/resource/data/" + StringUtils.Replace(this.getClassName(), ".", "/");
            this.files = [];
        }

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if ($POST.KeyExists("File")) {
                this.files.push($POST.getItem("File"));
            }
            if ($POST.KeyExists("Files")) {
                this.files.push($POST.getItem("Files"));
            }
            if ($POST.KeyExists("FileIds")) {
                this.fileIds = JSON.parse($POST.getItem("FileIds"));
            }
            this.maxFileSize = 0;
            if ($POST.KeyExists("MaxFileSize")) {
                this.maxFileSize = $POST.getItem("MaxFileSize");
            }
            if ($GET.KeyExists("type")) {
                this.type = $GET.getItem("type");
            }
            if ($GET.KeyExists("clientId")) {
                this.clientId = $GET.getItem("clientId");
            }
            if ($POST.KeyExists("ClientId")) {
                this.clientId = $POST.getItem("ClientId");
            }
        }

        protected resolver() : void {
            const path : Types.NodeJS.path = require("path");
            const contentLength : number = StringUtils.ToInteger(this.getRequest().getHeaders().getItem("content-length"));
            const maxPostLength : number = 1024 * 1024 * 25;
            if (contentLength > maxPostLength) {
                LogIt.Debug("File size: {0} bytes, exceeds the allowed limit: {1}", contentLength, maxPostLength);
                const response : string = "{\"error\":\"File size exceeds the allowed limit (" + maxPostLength + ").\"}";
                this.sendResponse(
                    "<script type=\"text/javascript\">parent.postMessage(JSON.stringify(" + response + "), \"*\");</script>");
            } else {
                if (!ObjectValidator.IsEmptyOrNull(this.files)) {
                    const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                    if (this.files.length === 1) {
                        const file : IFileUploadProtocol = this.files[0];
                        const name : string = ObjectDecoder.Base64(file.name);
                        const path : string = this.uploadPath + "/" + file.id;
                        if (this.type === "Remove") {
                            LogIt.Debug("Delete {0} [{1}]", name, path);
                            fileSystem.Delete(path);
                        } else {
                            LogIt.Debug("Print file chunk {0} to {1} [{2}]", "" + file.index, name, path);
                            if (file.index === 0) {
                                fileSystem.Delete(path);
                            }
                            fileSystem.Write(path, ObjectDecoder.Base64(file.data), true);
                        }
                        this.sendResponse({
                                end  : file.end,
                                error: 0,
                                id   : file.id,
                                index: file.index,
                                name : ObjectEncoder.Base64(name, true),
                                size : file.size,
                                start: file.start,
                                type : "chunk/" + file.index
                            }
                        );
                    } else {
                        const files : any[] = [];
                        const post : any = this.files[0];
                        if (!ObjectValidator.IsArray(post.name)) {
                            post.name.id = this.fileIds[0];
                            files[0] = post.name;
                        } else {
                            for (let $fileIndex : number = 0; $fileIndex < post.name.length; $fileIndex++) {
                                files[$fileIndex] = {
                                    error  : post.error[$fileIndex],
                                    id     : this.fileIds[$fileIndex],
                                    name   : post.name,
                                    size   : post.size[$fileIndex],
                                    tmpName: post.tmp_name[$fileIndex],
                                    type   : post.type[$fileIndex]
                                };
                            }
                        }

                        const processedFiles : any[] = [];
                        let fileIndex : number = 0;
                        files.forEach(($file : any) : void => {
                            const tmpName : string = $file.tmpName;
                            const error : number = $file.error;
                            const name : string = path.basename($file.name);
                            const fileId : number = $file.id;
                            const uploadPath : string = this.uploadPath + "/" + fileId;

                            if (ObjectValidator.IsEmptyOrNull(tmpName)) {
                                // $error = UPLOAD_ERR_NO_TMP_DIR;
                                LogIt.Error("Upload error. Temp path is empty.");
                            } else {
                                if (this.maxFileSize > 0 && $file.size > this.maxFileSize) {
                                    // $error = UPLOAD_ERR_FORM_SIZE;
                                    LogIt.Debug("File size: {0} bytes, exceeds the allowed limit: {1}", $file.size, this.maxFileSize);
                                } else {
                                    fileSystem.Delete(uploadPath);
                                    // if ($error === UPLOAD_ERR_OK) {
                                    //     if (move_uploaded_file($tmpName, $path)) {
                                    //         LogIt.Debug("Save file {0} [{1}]", $name, $path);
                                    //     } else {
                                    //         $error = 5;
                                    //         LogIt.Error("Possible file upload attack at " + $path + ".");
                                    //     }
                                    // } else {
                                    //     LogIt.Error("Upload error. [" + $error + "] at " + $path + ".");
                                    // }
                                }
                            }

                            processedFiles[fileIndex++] = {
                                error,
                                id  : fileId,
                                name: ObjectEncoder.Base64(name, true),
                                size: $file.size,
                                type: $file.type
                            };
                        });
                        this.sendResponse("{\"files\":" + ObjectEncoder.Utf8(JSON.stringify(processedFiles)) + "}");
                    }
                } else if (this.type === "Response") {
                    const persistence : IPersistenceHandler = PersistenceFactory.getPersistence(this.clientId);
                    const response : any = persistence.Variable("response");
                    persistence.Clear();
                    const header : any = {
                        /* tslint:disable: object-literal-sort-keys */
                        "Pragma"       : "public",
                        "Cache-Control": "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, private",
                        "Expires"      : "0",
                        "Accept-Ranges": "bytes",
                        "Content-Type" : "application/javascript"
                        /* tslint:enable */
                    };
                    this.sendResponse("JsonpData(" + response + ");");
                } else {
                    LogIt.Error("Unable to upload file, because POST data with key \"File\" or \"Files[]\" has not been found.");
                }
            }
        }

        private sendResponse($value) : void {
            const response : any = this.RequestArgs().POST().getItem(LiveContentWrapper.ClassName());
            if (!ObjectValidator.IsEmptyOrNull(response)) {
                response.data = $value;
            } else if (this.RequestArgs().POST().KeyExists("ClientId")) {
                const persistence : IPersistenceHandler = PersistenceFactory.getPersistence(this.RequestArgs().POST().getItem("ClientId"));
                persistence.ExpireTime("30 s");
                persistence.Variable("response", $value);
            } else {
                this.sendResponse("<script type=\"text/javascript\">parent.postMessage(JSON.stringify(" + $value + "), \"*\");</script>");
            }
        }
    }
}
