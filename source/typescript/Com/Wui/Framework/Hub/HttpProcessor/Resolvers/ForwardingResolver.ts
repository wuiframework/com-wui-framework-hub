/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpMethodType = Com.Wui.Framework.Commons.Enums.HttpMethodType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import IWebServiceRequestFormatterData = Com.Wui.Framework.Commons.Interfaces.IWebServiceRequestFormatterData;

    export class ForwardingResolver extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.BaseHttpResolver {
        private static requestBuffer : any = [];
        private static responseBuffer : any = [];
        private static processingRequest : boolean = false;
        private static processingResponse : boolean = false;
        private static tunnels : IResponse[] = [];

        public static RegisterTunnel($capabilities : string[], $callback : IResponse) : void {
            const tunnel : IResponse = ResponseFactory.getResponse($callback);
            $capabilities.forEach(($capability : string) : void => {
                ForwardingResolver.tunnels[$capability] = tunnel;
            });
        }

        public static CreateTunnels() : void {
            const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                Loader.getInstance().getAppConfiguration().tunnels.hubLocation + "/connector.config.jsonp");

            client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                LogIt.Warning($eventArgs.Exception().ToString("", false));
            });
            const capabilities : string[] = [];
            const tunnels : string[] = Loader.getInstance().getAppConfiguration().tunnels.agent;
            let tunnelName : string;
            for (tunnelName in tunnels) {
                if (tunnels.hasOwnProperty(tunnelName)) {
                    capabilities.push(tunnelName);
                }
            }
            if (ObjectValidator.IsEmptyOrNull(capabilities)) {
                LogIt.Error("Hub in agent mode cannot be started due to missing capabilities. " +
                    "Please, validated configuration at tunnels.agent section.");
            }

            const getNextResponse : any = ($index : number) : void => {
                if ($index < ForwardingResolver.responseBuffer.length) {
                    const request : any = ForwardingResolver.responseBuffer[$index];
                    LogIt.Debug("> forwarding {0}: {1}{2}", request.method, request.address, request.path);

                    request.headers["accept-encoding"] = "";
                    request.headers.host = StringUtils.Remove(request.address, "https://", "http://");
                    request.headers.origin = request.address;
                    request.headers.referer = request.address + request.path;

                    const options : any = require("url").parse(request.address + request.path);
                    options.method = request.method;
                    options.headers = request.headers;
                    options.rejectUnauthorized = false;
                    const makeRequest : any = () : void => {
                        const httpRequest : Types.NodeJS.Modules.http.ClientRequest =
                            (options.protocol === "https:" ? require("https") : require("http"))
                                .request(options, ($httpResponse : any) : void => {
                                    const data : any = [];
                                    $httpResponse.on("data", ($chunk : string) : void => {
                                        data.push($chunk);
                                    });
                                    $httpResponse.on("end", () : void => {
                                        try {
                                            request.protocol.data = ObjectEncoder.Base64(JSON.stringify([
                                                $httpResponse.statusCode, $httpResponse.headers, Buffer.concat(data).toString("base64")
                                            ]));
                                            client.Send(request.protocol);
                                        } catch (ex) {
                                            LogIt.Warning(ex.stack);
                                        }
                                        getNextResponse($index + 1);
                                    });
                                    $httpResponse.on("close", () : void => {
                                        LogIt.Debug("Proxy request closed.");
                                        getNextResponse($index + 1);
                                    });
                                    $httpResponse.on("error", ($ex : Error) : void => {
                                        LogIt.Warning($ex.stack);
                                        getNextResponse($index + 1);
                                    });
                                });
                        httpRequest.on("error", ($ex : Error) : void => {
                            if (StringUtils.Contains($ex.message, "ETIMEDOUT")) {
                                LogIt.Debug("Resend proxy request due to response timeout.");
                                makeRequest();
                            } else if (StringUtils.Contains($ex.message, "ENOTFOUND")) {
                                LogIt.Debug("Proxy host " + request.address + " was not found or is unavailable.");
                                request.protocol.data = ObjectEncoder.Base64(JSON.stringify([
                                    HttpStatusType.NOT_FOUND, {}, Buffer.from("Site is unavailable").toString("base64")
                                ]));
                                client.Send(request.protocol);
                                getNextResponse($index + 1);
                            } else {
                                LogIt.Warning("Proxy " + $ex.stack);
                                request.protocol.data = ObjectEncoder.Base64(JSON.stringify([
                                    HttpStatusType.ERROR, {}, Buffer.from($ex.message).toString("base64")
                                ]));
                                client.Send(request.protocol);
                                getNextResponse($index + 1);
                            }
                        });
                        if (request.method === HttpMethodType.POST && !ObjectValidator.IsEmptyOrNull(request.body)) {
                            httpRequest.write(request.body);
                        }
                        httpRequest.end();
                    };
                    makeRequest();
                } else {
                    ForwardingResolver.processingResponse = false;
                    ForwardingResolver.responseBuffer = [];
                }
            };

            let rawData : any;
            client.setRequestFormatter(($data : IWebServiceRequestFormatterData) : void => {
                if (ObjectValidator.IsObject($data.value)) {
                    if (ObjectValidator.IsEmptyOrNull($data.value.id)) {
                        $data.value.id = StringUtils.getCrc(client.getUID());
                        $data.value.origin = Loader.getInstance().getHttpManager().getRequest().getBaseUrl();
                        $data.value.status = HttpStatusType.CONTINUE;
                        $data.key = $data.value.id;
                    }
                    rawData = JSON.parse(JSON.stringify($data.value));
                }
            });
            LiveContentWrapper.InvokeMethod(client, ForwardingResolver.ClassName(), "RegisterTunnel", capabilities)
                .Then(($host : string, $path : string, $method : HttpMethodType, $headers : any, $body : string) : void => {
                    const clients : string[] = Loader.getInstance().getAppConfiguration().tunnels.agent;
                    if (clients.hasOwnProperty($host)) {
                        ForwardingResolver.responseBuffer.push({
                            address : clients[$host],
                            body    : $body,
                            headers : $headers,
                            method  : $method,
                            path    : $path,
                            protocol: JSON.parse(JSON.stringify(rawData))
                        });
                        if (!ForwardingResolver.processingResponse) {
                            ForwardingResolver.processingResponse = true;
                            getNextResponse(0);
                        }
                    } else {
                        LogIt.Warning("Unable to forward request to " + $host + ". Tunnel has not been found.");
                    }
                });
        }

        public Process() : void {
            this.resolver();
        }

        protected resolver() : void {
            const tunnels : string[] = Loader.getInstance().getAppConfiguration().tunnels.server;
            const headers : ArrayList<string> = this.getRequest().getHeaders();
            let host : string = headers.getItem("host");
            if (!ObjectValidator.IsEmptyOrNull(headers.getItem("x-forwarded-host"))) {
                host = headers.getItem("x-forwarded-host");
            }
            if (StringUtils.ContainsIgnoreCase(host, ":")) {
                host = StringUtils.Substring(host, 0, StringUtils.IndexOf(host, ":"));
            }
            let tunnel : IResponse = null;
            if (tunnels.hasOwnProperty(host) && ForwardingResolver.tunnels.hasOwnProperty(tunnels[host])) {
                tunnel = ForwardingResolver.tunnels[tunnels[host]];
            }
            if (!ObjectValidator.IsEmptyOrNull(tunnel)) {
                const getNextRequest : any = ($index : number) : void => {
                    if ($index < ForwardingResolver.requestBuffer.length) {
                        const request : any = ForwardingResolver.requestBuffer[$index];
                        tunnel
                            .Send(request.address, request.path, request.method, request.headers, request.body)
                            .Then(($status : HttpStatusType, $headers : any, $stream : string) : void => {
                                const Readable : any = require("stream").Readable;
                                const body : Types.NodeJS.Modules.stream.Readable = new Readable();
                                body._read = () : void => {
                                    // mock _read
                                };
                                body.push(Buffer.from($stream, "base64"));
                                body.push(null);
                                $headers["content-encoding"] = "";
                                if ($headers.hasOwnProperty("set-cookie") && !StringUtils.Contains(request.host, "https://")) {
                                    for (let index : number = 0; index < $headers["set-cookie"].length; index++) {
                                        $headers["set-cookie"][index] = StringUtils.Remove($headers["set-cookie"][index], "; secure");
                                    }
                                }
                                request.connector.Send({
                                    body,
                                    headers: $headers,
                                    status : $status
                                });
                                getNextRequest($index + 1);
                            });
                    } else {
                        ForwardingResolver.processingRequest = false;
                        ForwardingResolver.requestBuffer = [];
                    }
                };

                const path : string = this.RequestArgs().Url() === "/forward" ? "/" : this.getRequest().getOwner().url;
                if (path === "/robots.txt") {
                    this.getConnector().Send({
                        body   : "User-agent: *\nDisallow: /\n",
                        headers: {
                            "content-type": "text/plain"
                        },
                        status : HttpStatusType.SUCCESS
                    });
                } else {
                    let forwardHeaders : any = {
                        "user-agent": this.getRequest().getHeaders().getItem("user-agent")
                    };
                    let method : HttpMethodType = HttpMethodType.GET;
                    if (this.RequestArgs().Url() !== "/forward") {
                        forwardHeaders = this.getRequest().getOwner().headers;
                        method = this.getRequest().getOwner().method;
                    }

                    ForwardingResolver.requestBuffer.push({
                        address  : tunnels[host],
                        body     : JSON.parse(JSON.stringify(this.getProtocol())),
                        connector: this.getConnector(),
                        headers  : forwardHeaders,
                        host,
                        method,
                        path
                    });

                    if (!ForwardingResolver.processingRequest) {
                        ForwardingResolver.processingRequest = true;
                        getNextRequest(0);
                    }
                }
            } else {
                if (ObjectValidator.IsEmptyOrNull(host)) {
                    this.getConnector().Send({
                        body  : "Unable to resolve host address for required tunnel. " +
                            "Please, validate DNS settings or internet connection.",
                        status: HttpStatusType.NOT_FOUND
                    });
                } else {
                    this.getConnector().Send({
                        body  : "Tunnel for " + host + " has not been found or is not available.",
                        status: HttpStatusType.NOT_FOUND
                    });
                }
            }
        }
    }
}
