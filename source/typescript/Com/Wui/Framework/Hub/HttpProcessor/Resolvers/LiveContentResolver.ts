/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import UserManager = Com.Wui.Framework.Hub.Utils.UserManager;

    export class LiveContentResolver extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.LiveContentResolver {

        protected isAuthorizedFor($token : string, $requiredRoles : string | string[]) : boolean {
            // return UserManager.getInstance<UserManager>().IsAuthorizedFor($token, $requiredRoles);
            return true;
        }
    }
}
