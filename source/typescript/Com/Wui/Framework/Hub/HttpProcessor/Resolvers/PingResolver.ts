/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;

    export class PingResolver extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.BaseHttpResolver {

        public Process() : void {
            this.resolver();
        }

        protected resolver() : void {
            let url : string = this.getRequest().getUrl();
            if (StringUtils.Contains(url, "?")) {
                url = StringUtils.Substring(url, 0, StringUtils.IndexOf(url, "?"));
            }
            switch (url) {
            case "/ping.txt":
                this.getConnector().Send({
                    body   : "",
                    headers: {
                        "content-length": 0,
                        "content-type"  : "text/plain"
                    },
                    status : HttpStatusType.SUCCESS
                });
                break;
            case  "/ping.png":
                const Readable : any = require("stream").Readable;
                const body : Types.NodeJS.Modules.stream.Readable = new Readable();
                body._read = () : void => {
                    // mock _read
                };
                body.push(Buffer.from(
                    "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAB3RJTUUH4gUHBDgSIUDCjwAAAAxJREFUCNdj+P" +
                    "//PwAF/gL+3MxZ5wAAAABJRU5ErkJggg==", "base64"));
                body.push(null);
                this.getConnector().Send({
                    body,
                    headers: {
                        "content-length": 118,
                        "content-type"  : "image/png"
                    },
                    status : HttpStatusType.SUCCESS
                });
                break;
            default:
                this.getConnector().Send({
                    body  : "Unrecognized ping request.",
                    status: HttpStatusType.NOT_FOUND
                });
                break;
            }
        }
    }
}
