/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;

    export class PluginsRepository extends FileRequestResolver {
        private descriptor : string;
        private pluginName : string;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            super.argsHandler($GET, $POST);
            if ($GET.KeyExists("repoDescriptor")) {
                this.descriptor = $GET.getItem("repoDescriptor");
            }
            if ($GET.KeyExists("pluginName")) {
                this.pluginName = $GET.getItem("pluginName");
            }
        }

        protected resolver() : void {
            super.resolver();
            if (this.descriptor === "content.jar" || this.descriptor === "artifacts.jar") {
                this.getConnector().Send({
                    state: HttpStatusType.NOT_FOUND
                });
            } else {
                let filePath : string = Loader.getInstance().getProgramArgs().AppDataPath() +
                    "/resource/data/Com/Wui/Framework/Rest/Services/Templates/Eclipse/" + this.descriptor;
                if (!ObjectValidator.IsEmptyOrNull(this.pluginName)) {
                    filePath += "/" + this.pluginName;
                }
                const fs : Types.NodeJS.fs = require("fs");
                const stats : any = fs.statSync(filePath);
                const lastModifiedTime : number = stats.mtime.getTime();
                const lastModifiedTimeGMT : string = Convert.TimeToGMTformat(lastModifiedTime);
                const fileSize : number = stats.size;
                LogIt.Debug("Get content for: {0}", filePath);
                if (this.descriptor === "content.xml" || this.descriptor === "artifacts.xml") {
                    this.getConnector().Send({
                        body   : fs.createReadStream(filePath),
                        headers: {
                            /* tslint:disable: object-literal-sort-keys */
                            "pragma"       : "public",
                            "expires"      : "0",
                            "cache-control": "no-store, no-cache, must-revalidate, post-check=0, pre-check=0",
                            "accept-ranges": "bytes",
                            "content-type" : "application/xml",
                            "Last-Modified": lastModifiedTimeGMT
                            /* tslint:enable */
                        }
                    });
                } else {
                    this.getConnector().Send({
                        body   : fs.createReadStream(filePath),
                        headers: {
                            /* tslint:disable: object-literal-sort-keys */
                            "pragma"                   : "public",
                            "expires"                  : "0",
                            "cache-control"            : "no-store, no-cache, must-revalidate, post-check=0, pre-check=0",
                            "accept-ranges"            : "bytes",
                            "content-type"             : "application/java-archive",
                            "content-transfer-encoding": "binary",
                            "content-description"      : "File Transfer",
                            "content-length"           : fileSize,
                            "content-range"            : "0-" + (fileSize - 1) + "/" + fileSize,
                            "content-disposition"      : "attachment; filename=\"content.jar\"",
                            "x-content-type-options"   : "nosniff"
                            /* tslint:enable */
                        }
                    });
                }
            }
        }
    }
}
