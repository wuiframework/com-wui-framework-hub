/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Interfaces {
    "use strict";
    import ICertsConfiguration = Com.Wui.Framework.Localhost.Interfaces.ICertsConfiguration;

    export abstract class IProject extends Com.Wui.Framework.Localhost.Interfaces.IProject {
        public sendmail : ISendmailConfiguration;
        public tunnels : ITunnelsConfiguration;
        public certs : ICertsHubConfiguration;
    }

    export abstract class ITunnelsConfiguration {
        public hubLocation : string;
        public server : string[];
        public agent : string[];
    }

    export abstract class ISendmailConfiguration {
        public pass : string;
        public user : string;
        public location : string;
        public port : number;
    }

    export abstract class ICertsHubConfiguration extends ICertsConfiguration {
        public whitelist : string[];
    }
}
