/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub {
    "use strict";
    import HttpResolver = Com.Wui.Framework.Hub.HttpProcessor.HttpResolver;
    import IProject = Com.Wui.Framework.Hub.Interfaces.IProject;
    import HttpServer = Com.Wui.Framework.Hub.HttpProcessor.HttpServer;
    import ProgramArgs = Com.Wui.Framework.Hub.Structures.ProgramArgs;
    import ForwardingResolver = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.ForwardingResolver;

    export class Loader extends Com.Wui.Framework.Localhost.Loader {

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        public getAppConfiguration() : IProject {
            return <IProject>super.getAppConfiguration();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        public getProgramArgs() : ProgramArgs {
            return <ProgramArgs>super.getProgramArgs();
        }

        protected initProgramArgs() : ProgramArgs {
            return new ProgramArgs();
        }

        protected initHttpServer() : HttpServer {
            return new HttpServer();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver("/");
        }

        protected processProgramArgs() : boolean {
            if (this.getProgramArgs().IsAgent()) {
                this.getProgramArgs().StartServer(false);
            }
            let processed : boolean = super.processProgramArgs();
            if (this.getProgramArgs().IsAgent() && !processed) {
                processed = true;
                process.stdout.write(this.printLogo());
                ForwardingResolver.CreateTunnels();
            }
            return processed;
        }
    }
}
