/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Models {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class User extends BaseObject {
        private id : string;
        private username : string;
        private password : string;
        private role : string;

        constructor() {
            super();
            this.password = "";
            this.role = "User";
        }

        public Id($value? : string) : string {
            return this.id = Property.String(this.id, $value);
        }

        public Name($value? : string) : string {
            return this.username = Property.String(this.username, $value);
        }

        public Password($value? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.password = StringUtils.getSha1(this.username + $value);
            }
            if (ObjectValidator.IsEmptyOrNull(this.password)) {
                this.password = "";
            }
            return this.password;
        }

        public Role($value? : string) : string {
            return this.role = Property.String(this.role, $value);
        }
    }
}
