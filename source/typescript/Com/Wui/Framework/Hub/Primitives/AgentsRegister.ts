/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Primitives {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import IAgentInfo = Com.Wui.Framework.Services.Connectors.IAgentInfo;

    export class AgentsRegister extends BaseObject {
        protected static agents : IWuiAgent[] = <any>{};
        protected static tasks : IResponse[] = <any>{};

        public static RegisterAgent($capabilities : IWuiAgent, $callback? : IResponse) : void {
            const hash : string = StringUtils.getSha1($capabilities.domain + $capabilities.platform);
            this.agents[hash] = $capabilities;
            LogIt.Info("Registered builder[" + hash + "] with capabilities: " + JSON.stringify($capabilities));
            $capabilities.owner = this;
            $capabilities.time = new Date().getTime();
            $capabilities.connection = ResponseFactory.getResponse($callback);
            $capabilities.connection.Send(true);
            $capabilities.connection.AddAbortHandler(() : void => {
                LogIt.Warning("Builder[" + hash + "] has been disconnected. Removing from register ...");
                delete this.agents[hash];
            });
        }

        public static ForwardMessage($targetId : string, $data : any, $callback? : IResponse) : void {
            if (this.tasks.hasOwnProperty($targetId)) {
                this.tasks[$targetId].OnChange($data);
                ResponseFactory.getResponse($callback).Send(true);
            } else if (this.agents.hasOwnProperty($targetId)) {
                this.agents[$targetId].connection.OnChange($data);
                ResponseFactory.getResponse($callback).Send(true);
            } else {
                ResponseFactory.getResponse($callback).Send(false);
            }
        }

        public static getAgentsList($callback? : IResponse) : IAgentInfo[] {
            let hash : string;
            const list : IAgentInfo[] = [];
            for (hash in this.agents) {
                if (this.agents.hasOwnProperty(hash)) {
                    list.push({
                        domain   : this.agents[hash].domain,
                        name     : this.agents[hash].name,
                        platform : this.agents[hash].platform,
                        startTime: Convert.TimeToGMTformat(this.agents[hash].time),
                        version  : this.agents[hash].version
                    });
                }
            }
            ResponseFactory.getResponse($callback).Send(list);
            return list;
        }
    }

    export abstract class IWuiAgent {
        public name : string;
        public platform : string;
        public domain : string;
        public connection : IResponse;
        public version : string;
        public time : number;
        public owner : any;
    }
}
