/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Primitives {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;

    export class BaseRegister<TConfigItem> extends BaseConnector {

        protected getStoragePath() : string {
            const path : string = Loader.getInstance().getProgramArgs().AppDataPath() +
                "/resource/data/" + StringUtils.Replace(this.getClassName(), ".", "/");
            this.getStoragePath = () : string => {
                return path;
            };
            return path;
        }

        protected getRegisterPath() : string {
            const path : string = this.getStoragePath() + "/Register.json";
            this.getRegisterPath = () : string => {
                return path;
            };
            return path;
        }

        protected getRegister() : TConfigItem[] {
            const path : string = this.getRegisterPath();
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists(path)) {
                return JSON.parse(<string>fileSystem.Read(path));
            }
            return [];
        }
    }
}
