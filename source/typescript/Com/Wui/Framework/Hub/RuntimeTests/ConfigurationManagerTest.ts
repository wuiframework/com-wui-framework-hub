/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Hub.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import ConfigurationsManager = Com.Wui.Framework.Hub.Utils.ConfigurationsManager;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class ConfigurationManagerTestHandler extends Com.Wui.Framework.Localhost.Primitives.BaseConnector {

        @Extern()
        public static DownloadOverHttp($url : string, $callback : IResponse) : void {
            LogIt.Debug("download: {0}", $url);
            Loader.getInstance().getFileSystemHandler().Download(<any>{
                streamOutput: true,
                url         : $url
            }, ($headers : string, $bodyOrPath : string) : void => {
                ResponseFactory.getResponse($callback).Send($headers, $bodyOrPath);
            });
        }
    }

    export class ConfigurationManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private client : IWebServiceClient;

        constructor() {
            super();

            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Exception().ToString());
            });
        }

        public testUpload() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, ConfigurationsManager.ClassName(), "Upload",
                    "testApp", "testConfig.jsonp", "2018.1.0", JSON.stringify({
                        test: "testData"
                    }))
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, true);
                        $done();
                    });
            };
        }

        public testDownloadWS() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, ConfigurationsManager.ClassName(), "Download",
                    "testApp", "testConfig.jsonp", "2018.1.0")
                    .Then(($status : boolean, $config : any) : void => {
                        this.assertEquals($status, true);
                        Echo.Printf($config);
                        $done();
                    });
            };
        }

        public testDownloadHttp() : IRuntimeTestPromise {
            const url : string = this.getRequest().getHostUrl() + "Configuration/testApp/testConfig/2018.1.0";
            Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, ConfigurationManagerTestHandler.ClassName(), "DownloadOverHttp",
                    url + "?refresh=true")
                    .Then(($headers : string, $body : string) : void => {
                        Echo.Printf("{0} {1}", $headers, $body);
                        $done();
                    });
            };
        }
    }
}
/* dev:end */
