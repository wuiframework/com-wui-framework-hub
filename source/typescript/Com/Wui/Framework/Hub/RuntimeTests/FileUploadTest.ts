/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Hub.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import FileUploadHandler = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileUploadHandler;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;

    export class FileUploadTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private client : IWebServiceClient;

        constructor() {
            super();

            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Exception().ToString());
            });
        }

        public testUpload() : IRuntimeTestPromise {
            const url : string = this.getRequest().getHostUrl() + "Download/testFile.txt";
            Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, FileUploadHandler.ClassName(), "Upload",
                    {
                        data : ObjectEncoder.Base64("test data"),
                        id   : "testFile.txt",
                        index: 0,
                        name : "testFile.txt"
                    })
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, true);
                        $done();
                    });
            };
        }

        public testProxy() : void {
            const url : string = "https://localhost.wuiframework.com/Download/" +
                ObjectEncoder.Base64("http://www.angusj.com/resourcehacker/resource_hacker.zip");
            Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
        }
    }
}
/* dev:end */
