/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Hub.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import ReportAPI = Com.Wui.Framework.Hub.Utils.ReportAPI;
    import IReportProtocol = Com.Wui.Framework.Services.Connectors.IReportProtocol;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class ReportAPITest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private client : IWebServiceClient;

        constructor() {
            super();

            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Exception().ToString());
            });
        }

        public testReportMessage() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, ReportAPI.ClassName(), "LogIt",
                    "Test message"
                ).Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
            };
        }

        public testReportCrash() : IRuntimeTestPromise {
            const url : string = this.getRequest().getHostUrl() + "#/report/" + StringUtils.getSha1(
                this.getEnvironmentArgs().getProjectName() + this.getEnvironmentArgs().getProjectVersion());
            Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");

            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, ReportAPI.ClassName(), "CreateReport",
                    <IReportProtocol>{
                        appId      : StringUtils.getSha1(this.getEnvironmentArgs().getProjectName() +
                            this.getEnvironmentArgs().getProjectVersion()),
                        appName    : this.getEnvironmentArgs().getProjectName(),
                        appVersion : this.getEnvironmentArgs().getProjectVersion(),
                        log        : "some log",
                        printScreen: "",
                        timeStamp  : new Date().getTime(),
                        trace      : "some stack trace"
                    },
                    false
                ).Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
            };
        }
    }
}
/* dev:end */
