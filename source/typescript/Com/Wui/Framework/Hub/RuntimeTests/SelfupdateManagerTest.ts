/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Hub.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import SelfupdatesManager = Com.Wui.Framework.Hub.Utils.SelfupdatesManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class SelfupdateManagerTestHandler extends Com.Wui.Framework.Localhost.Primitives.BaseConnector {

        @Extern()
        public static DownloadOverHttp($url : string, $callback : IResponse) : void {
            LogIt.Debug("download: {0}", $url);
            Loader.getInstance().getFileSystemHandler().Download(<any>{
                streamOutput: true,
                url         : $url
            }, ($headers : string, $bodyOrPath : string) : void => {
                ResponseFactory.getResponse($callback).Send($headers, $bodyOrPath);
            });
        }

        @Extern()
        public static UploadTestPackage($projectName : string, $releaseName : string, $platform : string, $projectVersion : string,
                                        $buildTime : number, $type : string, $callback : IResponse) : void {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const filePath : string = fileSystem.NormalizePath(Loader.getInstance().getProgramArgs().ProjectBase() + "/../" +
                $projectName + "-" +
                StringUtils.Replace(Loader.getInstance().getEnvironmentArgs().getProjectVersion(), ".", "-") + ".zip");

            if (fileSystem.Exists(filePath)) {
                const maxChunkSize : number = 1024 * 1024 * 1.5;
                const path : Types.NodeJS.path = require("path");
                const fs : Types.NodeJS.fs = require("fs");
                const {Transform} = require("stream");
                let chunkIndex : number = 0;
                const contentSize : number = fs.statSync(filePath).size;

                const chunk : any = {
                    data : "",
                    end  : maxChunkSize,
                    id   : path.basename(filePath),
                    index: chunkIndex,
                    name : ObjectEncoder.Base64(path.basename(filePath)),
                    size : contentSize,
                    start: 0
                };

                try {
                    LogIt.Info("Uploading file \"" + filePath + "\"");

                    chunk.id = (new Date()).getTime() + "";

                    let buffer : Buffer = null;
                    let sendChunk : any;
                    const pipeTransform : typeof Transform = new Transform({
                        transform($data : any, $encoding : string, $callback : any) : void {
                            if (buffer === null) {
                                buffer = Buffer.from($data);
                            } else if (buffer.byteLength < maxChunkSize) {
                                buffer = Buffer.concat([buffer, Buffer.from($data)]);
                            }
                            if (buffer.byteLength >= maxChunkSize) {
                                sendChunk($callback);
                            } else {
                                $callback();
                            }
                        }
                    });
                    sendChunk = ($callback : any) : void => {
                        if (buffer !== null && buffer.byteLength !== 0) {
                            chunk.index = chunkIndex;
                            chunk.data = buffer.toString("base64");
                            chunk.end = chunk.start + maxChunkSize;
                            if (chunk.end > chunk.size) {
                                chunk.end = chunk.size;
                            }

                            LogIt.Info("Sending chunk #" + chunkIndex +
                                " (" + Convert.IntegerToSize(chunk.end) + "/" + Convert.IntegerToSize(chunk.size) + ")");
                            SelfupdatesManager.Upload(chunk, <any>(($status : boolean) : void => {
                                if ($status) {
                                    chunk.start = chunk.end;
                                    chunkIndex++;
                                    buffer = null;
                                    $callback();
                                } else {
                                    ResponseFactory.getResponse($callback).OnError("Chunk upload has failed");
                                }
                            }));
                        } else {
                            $callback();
                        }
                    };

                    const stream : any = fs.createReadStream(filePath, <any>{
                        bufferSize: maxChunkSize
                    });
                    stream
                        .on("end", () : void => {
                            sendChunk(() : void => {
                                LogIt.Info("File has been uploaded successfully.");
                                SelfupdatesManager.Register(
                                    $projectName,
                                    $releaseName,
                                    $platform,
                                    $projectVersion,
                                    $buildTime,
                                    chunk.id,
                                    $type,
                                    $callback);
                            });
                        })
                        .on("error", ($error : Error) : void => {
                            ResponseFactory.getResponse($callback).OnError($error);
                            LogIt.Warning($error.message);
                        })
                        .pipe(pipeTransform);
                } catch (ex) {
                    ResponseFactory.getResponse($callback).OnError(ex);
                    LogIt.Warning(ex.message);
                }
            } else {
                const message : string = "File for upload does not exists: " + filePath;
                ResponseFactory.getResponse($callback).OnError(message);
                LogIt.Warning(message);
            }
        }
    }

    export class SelfupdateManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private client : IWebServiceClient;

        constructor() {
            super();

            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Exception().ToString());
            });
        }

        public testUploadVersion() : IRuntimeTestPromise {
            this.timeoutLimit(-1);
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                    this.getEnvironmentArgs().getProjectName(),
                    "Win",
                    "app-win-nodejs",
                    "2018.1.0-alpha",
                    1539853031794,
                    "zip")
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, true);
                        $done();
                    });
            };
        }

        public testUploadZip() : IRuntimeTestPromise {
            this.timeoutLimit(-1);
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                    this.getEnvironmentArgs().getProjectName(),
                    "Win",
                    "app-win-nodejs",
                    this.getEnvironmentArgs().getProjectVersion(),
                    new Date(this.getEnvironmentArgs().getBuildTime()).getTime(),
                    "zip")
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, true);
                        $done();
                    });
            };
        }

        public testUploadTar() : IRuntimeTestPromise {
            this.timeoutLimit(-1);
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                    this.getEnvironmentArgs().getProjectName(),
                    "Linux",
                    "app-linux-nodejs",
                    this.getEnvironmentArgs().getProjectVersion(),
                    new Date(this.getEnvironmentArgs().getBuildTime()).getTime(),
                    "tar.bz2")
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, true);
                        $done();
                    });
            };
        }

        public testUpdateExists() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, SelfupdatesManager.ClassName(), "UpdateExists",
                    this.getEnvironmentArgs().getProjectName(),
                    "Win",
                    "app-win-nodejs",
                    "2018.1.0-alpha",
                    1539853031794,
                    false)
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, true);
                        $done();
                    });
            };
        }

        public testUpdateNotExists() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, SelfupdatesManager.ClassName(), "UpdateExists",
                    this.getEnvironmentArgs().getProjectName(),
                    "Win",
                    "app-win-nodejs",
                    this.getEnvironmentArgs().getProjectVersion(),
                    new Date(this.getEnvironmentArgs().getBuildTime()).getTime(),
                    false)
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, false);
                        $done();
                    });
            };
        }

        public testDownloadHttp() : IRuntimeTestPromise {
            const environment : EnvironmentArgs = this.getEnvironmentArgs();
            const baseUrl : string = this.getRequest().getHostUrl() + "Update/" + environment.getProjectName();
            let url : string = baseUrl + "/Win/app-win-nodejs";
            Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
            url += "/2018.1.0-alpha";
            Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
            url = baseUrl + "/Linux/app-linux-nodejs";
            Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");

            return ($done : () => void) : void => {
                // LiveContentWrapper.InvokeMethod(this.client, this.getClassName(), "DownloadOverHttp",
                //     url)
                //     .Then(($headers : string, $body : string) : void => {
                //         Echo.Printf("{0} {1}", $headers, $body);
                //         $done();
                //     });
                $done();
            };
        }
    }
}
/* dev:end */
