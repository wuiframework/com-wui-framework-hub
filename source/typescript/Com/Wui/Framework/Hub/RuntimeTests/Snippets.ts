/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Hub.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import SendMail = Com.Wui.Framework.Hub.Utils.SendMail;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;

    export class Snippets extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private client : IWebServiceClient;

        constructor() {
            super();

            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Exception().ToString());
            });
        }

        public __IgnoretestSendMail() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, SendMail.ClassName(), "Send",
                    "jakub.cieslar@nxp.com",
                    "Test mail from WUI Hub",
                    "<h1>New TEST mail</h1>" +
                    "This is test message from WUI Mail sender<br>" +
                    "<p>Regards, WUI Reporter</p>" +
                    "<img src='/resource/graphics/Com/Wui/Framework/Commons/WUILogo.png'>",
                    ["/resource/graphics/Com/Wui/Framework/Commons/WUILogo.png"],
                    "Some important test mail from WUI Reporter",
                    "",
                    ""
                ).Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
            };
        }
    }
}
/* dev:end */
