/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Hub.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import UserManager = Com.Wui.Framework.Hub.Utils.UserManager;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import PersistenceType = Com.Wui.Framework.Commons.Enums.PersistenceType;
    import AuthHttpResolver = Com.Wui.Framework.Services.ErrorPages.AuthHttpResolver;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class UserManagerTestHandler extends Com.Wui.Framework.Localhost.Primitives.BaseConnector {

        @Extern()
        public static InitUsers($callback : IResponse) : void {
            const manager : UserManager = UserManager.getInstance();
            manager.Register("root", "pass");
            manager.Register("test", "welcome");
            ResponseFactory.getResponse($callback).Send(manager.setRole("root", "Admin"));
        }
    }

    export class UserManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private client : IWebServiceClient;

        constructor() {
            super();

            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                this.getRequest().getHostUrl() + "connector.config.jsonp");
            this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf($eventArgs.Exception().ToString());
            });
        }

        public testUserManager() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                LiveContentWrapper.InvokeMethod(this.client, UserManagerTestHandler.ClassName(), "InitUsers")
                    .Then(($status : boolean) : void => {
                        this.assertEquals($status, true);

                        LiveContentWrapper.InvokeMethod(this.client, UserManager.ClassName(), "LogIn",
                            "root", "pass"
                        ).Then(($token : string) : void => {
                            PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
                                .Variable("token", $token);

                            LiveContentWrapper.InvokeMethod(this.client, UserManager.ClassName(), "setRole",
                                "test", "Admin"
                            ).Then(($status : boolean) : void => {
                                this.assertEquals($status, true);
                                $done();
                            });
                        });
                    });
            };
        }
    }
}
/* dev:end */
