/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class ProgramArgs extends Com.Wui.Framework.Localhost.Structures.ProgramArgs {
        private isAgent : boolean;

        constructor() {
            super();
            this.isAgent = false;
        }

        public IsAgent($value? : boolean) : boolean {
            return this.isAgent = Property.Boolean(this.isAgent, $value);
        }

        public PrintHelp() : void {
            this.getHelp(
                "" +
                "WUI Framework Hub                                                            \n" +
                "   - Synchronization server for WUI Framework services                       \n" +
                "                                                                             \n" +
                "   copyright:         Copyright (c) 2017-2019 NXP                            \n" +
                "   author:            Jakub Cieslar, jakub.cieslar@nxp.com                   \n" +
                "   license:           BSD-3-Clause License distributed with this material    \n",

                "" +
                "Basic options:                                                               \n" +
                "   -h [ --help ]      Prints application help description.                   \n" +
                "   -v [ --version ]   Prints application version message.                    \n" +
                "   -p [ --path ]      Print current WUI Host location.                       \n" +
                "                                                                             \n" +
                "Server options:                                                              \n" +
                "   start              Start localhost service.                               \n" +
                "   stop               Stop localhost service.                                \n" +
                "   restart            Restart localhost service.                             \n" +
                "                                                                             \n" +
                "Other options:                                                               \n" +
                "   --target=<path>    Specify target *.html file to host by localhost.       \n" +
                "   --agent            Specify if current instance should be in agent role.   \n"
            );
        }

        public processArg($key : string, $value : string) : boolean {
            switch ($key) {
            case "--agent":
                this.IsAgent(true);
                break;
            default:
                return super.processArg($key, $value);
            }
            return true;
        }
    }
}
