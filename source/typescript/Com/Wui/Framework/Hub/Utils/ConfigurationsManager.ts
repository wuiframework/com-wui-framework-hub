/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import ConfigFile = Com.Wui.Framework.Hub.Models.ConfigFile;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import BaseRegister = Com.Wui.Framework.Hub.Primitives.BaseRegister;
    import IFilePath = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.IFilePath;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class ConfigurationsManager extends BaseRegister<ConfigFile> {

        @Extern()
        public static Upload($appName : string, $configName : string, $version : string, $data : string, $callback? : IResponse) : void {
            this.pipe<ConfigurationsManager>($callback).Upload($appName, $configName, $version, $data);
        }

        @Extern()
        public static Download($appName : string, $configName : string, $version : string = null, $callback? : IResponse) : void {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const filePath : string = this.getInstance<ConfigurationsManager>().Download($appName, $configName, $version).path;
            if (!ObjectValidator.IsEmptyOrNull(filePath) && fileSystem.Exists(filePath)) {
                ResponseFactory.getResponse($callback).Send(true, fileSystem.Read(filePath).toString());
            } else {
                ResponseFactory.getResponse($callback).Send(false, "");
            }
        }

        public Upload($appName : string, $configName : string, $version : string, $data : string) : boolean {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            $configName = StringUtils.Remove($configName, ".jsonp");
            const newConfigFile : ConfigFile = new ConfigFile();
            newConfigFile.name = $appName;
            newConfigFile.version = $version;
            newConfigFile.configName = $configName;
            newConfigFile.timestamp = (new Date()).getTime();

            const register : ConfigFile[] = this.getRegister();

            let configExists : boolean = false;
            if (fileSystem.Write(this.getConfigPath(newConfigFile), $data)) {
                register.forEach(($configFile : ConfigFile) : void => {
                    if ($configFile.name === newConfigFile.name &&
                        $configFile.version === newConfigFile.version &&
                        $configFile.configName === newConfigFile.configName
                    ) {
                        $configFile.timestamp = newConfigFile.timestamp;
                        configExists = true;
                    }
                });
                if (!configExists) {
                    register.push(newConfigFile);
                }
                if (fileSystem.Write(this.getRegisterPath(), JSON.stringify(register))) {
                    LogIt.Debug("Register config file {0} {1}[{2}]", $appName, $configName, $version);
                    return true;
                }
            }
            return false;
        }

        public Download($appName : string, $configName : string, $version : string = null) : IFilePath {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            $configName = StringUtils.Remove($configName, ".jsonp");
            LogIt.Debug("Download config file {0} {1}[{2}]", $appName, $configName, $version);
            let filePath : string = "";
            const register : ConfigFile[] = this.getRegister();
            let lastVersion : string = "";
            let lastModified : number = 0;
            register.forEach(($configFile : ConfigFile) : void => {
                if ($configFile.name === $appName &&
                    $configFile.configName === $configName
                ) {
                    if ((!ObjectValidator.IsEmptyOrNull($version) && $configFile.version === $version) ||
                        (ObjectValidator.IsEmptyOrNull($version) && ObjectValidator.IsEmptyOrNull(lastVersion) ||
                            StringUtils.VersionIsLower(lastVersion, $configFile.version))
                    ) {
                        filePath = this.getConfigPath($configFile);
                        lastModified = $configFile.timestamp;
                        if (ObjectValidator.IsEmptyOrNull($version)) {
                            lastVersion = $configFile.version;
                        }
                    }
                }
            });
            if (fileSystem.Exists(filePath)) {
                const fileSize : number = require("fs").statSync(filePath).size;
                LogIt.Debug("Transferring file \"{0}\" with size: {1}", filePath, fileSize + "");
                return {
                    extension: "jsonp",
                    name     : $configName,
                    path     : filePath,
                    url      : ""
                };
            } else {
                LogIt.Debug("Configuration file \"{0}\" does not exist.", filePath);
                return {
                    extension: "jsonp",
                    name     : $configName,
                    path     : "",
                    url      : ""
                };
            }
        }

        public getRegisterLinks() : string[] {
            const links : string[] = [];
            this.getRegister().forEach(($configFile : ConfigFile) : void => {
                links.push("/Configuration/" + $configFile.name + "/" + $configFile.configName + "/" + $configFile.version);
            });
            return links;
        }

        private getConfigPath($config : ConfigFile) : string {
            return this.getStoragePath() + "/" + StringUtils.getSha1($config.name + $config.configName + $config.version);
        }
    }
}
