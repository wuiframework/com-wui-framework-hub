/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import IReportProtocol = Com.Wui.Framework.Services.Connectors.IReportProtocol;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class ReportAPI extends BaseConnector {
        private readonly storageBase : string;
        private readonly crashLogsPath : string;
        private readonly templatePath : string;
        private readonly mailListPath : string;

        @Extern()
        public static LogIt($message : string, $callback? : IResponse) : void {
            try {
                LogIt.Info(ObjectDecoder.Base64($message));
                ResponseFactory.getResponse($callback).Send(true);
            } catch (ex) {
                LogIt.Warning(ex.stack);
                ResponseFactory.getResponse($callback).Send(false);
            }
        }

        @Extern()
        public static CreateReport($data : IReportProtocol, $sendEmail : boolean = true, $callback? : IResponse) : void {
            this.getInstance<ReportAPI>().CreateReport($data, $sendEmail, $callback);
        }

        constructor() {
            super();
            const targetBase : string = Loader.getInstance().getProgramArgs().TargetBase();
            this.storageBase = Loader.getInstance().getProgramArgs().AppDataPath();
            this.crashLogsPath = "/resource/data/Com/Wui/Framework/Hub/Utils/ReportAPI/Logs";
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            this.templatePath = "/resource/data/Com/Wui/Framework/Hub/Templates/CrashReport.html";
            if (fileSystem.Exists(this.storageBase + this.templatePath)) {
                this.templatePath = this.storageBase + this.templatePath;
            } else {
                this.templatePath = targetBase + this.templatePath;
            }
            this.mailListPath = "/resource/data/Com/Wui/Framework/Hub/Configuration/CrashReportMailList.json";
            if (fileSystem.Exists(this.storageBase + this.mailListPath)) {
                this.mailListPath = this.storageBase + this.mailListPath;
            } else {
                this.mailListPath = targetBase + this.mailListPath;
            }
        }

        public CreateReport($data : IReportProtocol, $sendEmail : boolean, $callback : (($status : boolean) => void) | IResponse) : void {
            try {
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                const appName : string = ObjectDecoder.Base64($data.appName);
                const appVersion : string = ObjectDecoder.Base64($data.appVersion);

                const logDirectory : string = this.crashLogsPath + "/" + $data.appId;
                let logFilePath : string = "Without log file";
                let printScreenPath : string = "Without print screen";
                if (!ObjectValidator.IsEmptyOrNull($data.log)) {
                    logFilePath = logDirectory + "/" + $data.timeStamp + ".log";
                    const logFileContent : string = StringUtils.Format(
                        "================================================={2}" +
                        "                   STACK TRACE                   {2}" +
                        "================================================={2}{2}" +
                        "{0}{2}{2}" +
                        "================================================={2}" +
                        "                     APP LOG                     {2}" +
                        "================================================={2}{2}" +
                        "{1}{2}" +
                        "-------------------- LOG END --------------------{2}",
                        ObjectValidator.IsEmptyOrNull($data.trace) ? "Unavailable" :
                            StringUtils.Replace(StringUtils.Replace(ObjectDecoder.Base64($data.trace), "\r\n", "\n"),
                                "\n", StringUtils.NewLine(false)),
                        ObjectDecoder.Base64($data.log),
                        StringUtils.NewLine(false));
                    fileSystem.Write(this.storageBase + logFilePath, logFileContent);
                }
                if (!ObjectValidator.IsEmptyOrNull($data.printScreen)) {
                    printScreenPath = logDirectory + "/" + $data.timeStamp + ".png";
                    fileSystem.Write(this.storageBase + printScreenPath, Buffer.from($data.printScreen, "base64"));
                }
                const crashReport : string = <string>fileSystem.Read(this.templatePath);
                const crashTime : string = Convert.TimeToGMTformat(Convert.ToFixed($data.timeStamp / 1000, 0));

                if (!ObjectValidator.IsEmptyOrNull(crashReport)) {
                    const reportServiceLink : string =
                        Loader.getInstance().getHttpManager().getRequest().getHostUrl() + "#/report/" + $data.appId;
                    const body : string = StringUtils.Format(crashReport,
                        appName + " " + appVersion,
                        $data.appId,
                        reportServiceLink,
                        crashTime
                    );
                    const recipients : any = this.getRecipients(appName, appVersion);

                    LogIt.Debug("Application crash has been detected. At: {0}, from: {1}", crashTime, appName);
                    if ($sendEmail) {
                        SendMail.Send(
                            recipients.to,
                            "WUI application crash report, time: " + crashTime,
                            body,
                            [logFilePath, printScreenPath],
                            "This report is accessible online at: " + reportServiceLink,
                            recipients.cc,
                            "",
                            $callback);
                    } else {
                        ResponseFactory.getResponse($callback).Send(true);
                    }
                } else {
                    LogIt.Warning("CrashReport template has not been found or is empty.");
                    ResponseFactory.getResponse($callback).Send(false);
                }
            } catch (ex) {
                LogIt.Warning(ex.stack);
                ResponseFactory.getResponse($callback).Send(false);
            }
        }

        public getRecipients($appName : string, $appVersion : string) : any {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const to : string[] = [];
            const cc : string[] = [];

            if (!fileSystem.Exists(this.mailListPath)) {
                LogIt.Warning("Path to configuration file \"" + this.mailListPath + "\" does not exist.");
            } else {
                const configuration : any = JSON.parse(<string>fileSystem.Read(this.mailListPath));
                if (ObjectValidator.IsEmptyOrNull(configuration)) {
                    LogIt.Warning("Invalid configuration file \"" + this.mailListPath + "\"");
                }

                const appHash : string = StringUtils.getSha1($appName + $appVersion);

                let listForAppName : string;
                for (listForAppName in configuration) {
                    if (configuration.hasOwnProperty(listForAppName)) {
                        const versionOrMailList : any = configuration[listForAppName];
                        if (StringUtils.PatternMatched(listForAppName, $appName) || listForAppName === appHash) {
                            if (ObjectValidator.IsArray(versionOrMailList)) {
                                versionOrMailList.forEach(($mail : string) : void => {
                                    if (to.indexOf($mail) === -1) {
                                        to.push($mail);
                                    }
                                });
                            } else if (ObjectValidator.IsObject(versionOrMailList)) {
                                let listForVersion : string;
                                for (listForVersion in versionOrMailList) {
                                    if (versionOrMailList.hasOwnProperty(listForVersion)) {
                                        const mailListOrGroup : any = versionOrMailList[listForVersion];
                                        if (StringUtils.PatternMatched(listForVersion, $appVersion)) {
                                            if (ObjectValidator.IsArray(mailListOrGroup)) {
                                                mailListOrGroup.forEach(($mail : string) : void => {
                                                    if (to.indexOf($mail) === -1) {
                                                        to.push($mail);
                                                    }
                                                });
                                            } else if (ObjectValidator.IsObject(mailListOrGroup)) {
                                                if (ObjectValidator.IsSet(mailListOrGroup.to)) {
                                                    mailListOrGroup.to.forEach(($mail : string) : void => {
                                                        if (to.indexOf($mail) === -1) {
                                                            to.push($mail);
                                                        }
                                                    });
                                                }
                                                if (ObjectValidator.IsSet(mailListOrGroup.cc)) {
                                                    mailListOrGroup.cc.forEach(($mail : string) : void => {
                                                        if (cc.indexOf($mail) === -1) {
                                                            cc.push($mail);
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return {
                cc,
                to
            };
        }

        public getReports($appId : string) : string[] {
            return Loader.getInstance().getFileSystemHandler().Expand(this.storageBase + this.crashLogsPath + "/" + $appId + "/**/*");
        }

        public getContent($reportId : string) : string {
            let content : any = Loader.getInstance().getFileSystemHandler().Read($reportId);
            if (StringUtils.EndsWith($reportId, ".png")) {
                content = "data:image/png;base64," + content.toString("base64");
            } else {
                content = content.toString();
            }
            return content;
        }
    }

    export interface IReportsPromise {
        Then($callback : ($list : string[]) => void) : void;
    }

    export interface IReportContentPromise {
        Then($callback : ($value : string) => void) : void;
    }
}
