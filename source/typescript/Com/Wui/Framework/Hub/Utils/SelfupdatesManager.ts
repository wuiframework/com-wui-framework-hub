/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import SelfupdatePackage = Com.Wui.Framework.Hub.Models.SelfupdatePackage;
    import BaseRegister = Com.Wui.Framework.Hub.Primitives.BaseRegister;
    import IFilePath = Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.IFilePath;
    import IFileUploadProtocol = Com.Wui.Framework.Gui.Interfaces.Components.IFileUploadProtocol;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class SelfupdatesManager extends BaseRegister<SelfupdatePackage> {

        @Extern()
        public static Register($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                               $fileName : string, $type : string, $callback? : IResponse) : void {
            this.pipe<SelfupdatesManager>($callback).Register($appName, $releaseName, $platform, $version, $buildTime, $fileName, $type);
        }

        @Extern()
        public static UpdateExists($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                                   $fixedVersion : boolean = false, $callback? : IResponse) : void {
            if (!ObjectValidator.IsBoolean($fixedVersion)) {
                $callback = <any>$fixedVersion;
                $fixedVersion = false;
            }
            this.pipe<SelfupdatesManager>($callback).UpdateExists($appName, $releaseName, $platform, $version, $buildTime, $fixedVersion);
        }

        @Extern()
        public static Upload($data : IFileUploadProtocol, $callback? : IResponse) : void {
            this.pipe<SelfupdatesManager>($callback).Upload($data);
        }

        @Extern()
        public static Download($appName : string, $releaseName : string = "", $platform : string = "", $version : string = null,
                               $callback? : IResponse) : void {
            if (!ObjectValidator.IsString($releaseName)) {
                $callback = <any>$releaseName;
                $releaseName = "";
                $platform = "";
                $version = "";
            } else if (!ObjectValidator.IsString($platform)) {
                $callback = <any>$platform;
                $platform = "";
                $version = "";
            } else if (!ObjectValidator.IsString($version)) {
                $callback = <any>$version;
                $version = "";
            }
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const filePath : string = this.getInstance<SelfupdatesManager>().Download($appName, $releaseName, $platform, $version).path;
            if (!ObjectValidator.IsEmptyOrNull(filePath) && fileSystem.Exists(filePath)) {
                ResponseFactory.getResponse($callback).Send(true, fileSystem.Read(filePath).toString());
            } else {
                ResponseFactory.getResponse($callback).Send(false, "");
            }
        }

        public Register($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                        $fileName : string, $type : string) : boolean {
            $releaseName = ObjectValidator.IsEmptyOrNull($releaseName) || $releaseName === "null" ? "" : $releaseName;
            $platform = ObjectValidator.IsEmptyOrNull($platform) || $platform === "null" ? "" : $platform;
            $type = ObjectValidator.IsEmptyOrNull($type) || $type === "null" ? "zip" : $type;
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const newPackage : SelfupdatePackage = new SelfupdatePackage();
            newPackage.name = $appName;
            newPackage.release = $releaseName;
            newPackage.platform = $platform;
            newPackage.version = $version;
            newPackage.buildTime = $buildTime;
            newPackage.fileName = $fileName;
            newPackage.type = $type;
            const register : SelfupdatePackage[] = this.getRegister();
            let packageExists : boolean = false;
            register.forEach(($package : SelfupdatePackage) : void => {
                if ($package.name === newPackage.name &&
                    $package.release === newPackage.release &&
                    $package.platform === newPackage.platform &&
                    $package.version === newPackage.version
                ) {
                    fileSystem.Delete(this.getStoragePath() + "/" + $package.fileName);
                    $package.buildTime = newPackage.buildTime;
                    $package.fileName = newPackage.fileName;
                    $package.type = newPackage.type;
                    packageExists = true;
                }
            });
            if (!packageExists) {
                register.push(newPackage);
            }
            if (fileSystem.Write(this.getRegisterPath(), JSON.stringify(register))) {
                LogIt.Debug("Register selfupdate package {0}(\"{1}\",{2}) {3} {4}",
                    $appName, $releaseName, $platform, $version, $buildTime);
                return true;
            }
            return false;
        }

        public UpdateExists($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                            $fixedVersion : boolean = false) : boolean {
            $releaseName = ObjectValidator.IsEmptyOrNull($releaseName) || $releaseName === "null" ? "" : $releaseName;
            $platform = ObjectValidator.IsEmptyOrNull($platform) || $platform === "null" ? "" : $platform;
            LogIt.Debug("Validate selfupdate package {0}(\"{1}\",{2}) {3} {4}",
                $appName, $releaseName, $platform, $version, $buildTime);
            if ($fixedVersion) {
                LogIt.Warning("Required validation of selfupdate against fixed version.");
            }
            let exists : boolean = false;
            this.getRegister().forEach(($package : SelfupdatePackage) : boolean => {
                if ($package.name === $appName &&
                    $package.release === $releaseName &&
                    ($package.platform === $platform || $platform === "installer" && $package.platform === "win32")
                ) {
                    if ($package.version === $version) {
                        exists = $package.buildTime > $buildTime;
                    } else if (!$fixedVersion && StringUtils.VersionIsLower($version, $package.version)) {
                        exists = true;
                    }
                    return !exists;
                }
            });
            LogIt.Debug("Update exists: {0}", exists);
            return exists;
        }

        public Upload($data : IFileUploadProtocol) : boolean {
            const name : string = ObjectDecoder.Base64($data.name);
            const path : string = this.getStoragePath() + "/" + $data.id;
            LogIt.Debug("Print file chunk {0} to {1} [{2}]", "" + $data.index, name, path);
            if (ObjectValidator.IsString($data.data)) {
                $data.data = <any>Buffer.from($data.data, "base64");
            }
            return Loader.getInstance().getFileSystemHandler().Write(path, $data.data, $data.index !== 0);
        }

        public Download($appName : string, $releaseName : string = "", $platform : string = "",
                        $version : string = null) : ISelfupdatePackage {
            if (!ObjectValidator.IsString($releaseName) || ObjectValidator.IsEmptyOrNull($releaseName) || $releaseName === "null") {
                $releaseName = "";
            }
            if (!ObjectValidator.IsString($platform) || ObjectValidator.IsEmptyOrNull($platform) || $platform === "null") {
                $platform = "";
            }
            if (!ObjectValidator.IsString($version) || ObjectValidator.IsEmptyOrNull($version) || $version === "null") {
                $version = "";
            }
            LogIt.Debug("Download selfupdate package for {0}(\"{1}\",{2})", $appName, $releaseName, $platform);
            if (!ObjectValidator.IsEmptyOrNull($version)) {
                LogIt.Warning("Required download of selfupdate package for specific version \"" + $version + "\"");
            }
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            let filePath : string = "";
            let fileName : string = "";
            let type : string = "zip";
            let lastVersion : string = "";
            let lastModified : number = 0;
            this.getRegister().forEach(($package : SelfupdatePackage) : void => {
                if ($package.name === $appName &&
                    $package.release === $releaseName &&
                    ($package.platform === $platform || $platform === "installer" && $package.platform === "win32") &&
                    (ObjectValidator.IsEmptyOrNull($version) ||
                        !ObjectValidator.IsEmptyOrNull($version) && $package.version === $version)
                ) {
                    if (ObjectValidator.IsEmptyOrNull(lastVersion) || StringUtils.VersionIsLower(lastVersion, $package.version)) {
                        filePath = this.getStoragePath() + "/" + $package.fileName;
                        if (!ObjectValidator.IsEmptyOrNull($package.type)) {
                            type = $package.type;
                        }
                        fileName = $package.name + "-" + StringUtils.Replace($package.version, ".", "-") + "." + type;
                        lastVersion = $package.version;
                        lastModified = $package.buildTime;
                    }
                }
            });
            if (ObjectValidator.IsString(lastModified)) {
                lastModified = StringUtils.ToInteger(<any>lastModified);
            }
            if (fileSystem.Exists(filePath)) {
                const fileSize : number = require("fs").statSync(filePath).size;
                LogIt.Debug("Transferring file \"{0}\" with size: {1}", filePath, fileSize + "");
                return {
                    extension: type,
                    modified : lastModified,
                    name     : fileName,
                    path     : filePath,
                    url      : "",
                    version  : lastVersion
                };
            } else {
                if (ObjectValidator.IsEmptyOrNull(filePath)) {
                    LogIt.Debug("Required package does not exist.");
                } else {
                    LogIt.Debug("Package file \"{0}\" does not exist.", filePath);
                }
                return {
                    extension: "zip",
                    modified : lastModified,
                    name     : "",
                    path     : "",
                    url      : "",
                    version  : lastVersion
                };
            }
        }

        public getRegisterInfo() : SelfupdatePackage[] {
            const links : any[] = [];
            this.getRegister().forEach(($package : SelfupdatePackage) : void => {
                const platform : string = ObjectValidator.IsEmptyOrNull($package.platform) ? "null" : $package.platform;
                const release : string = ObjectValidator.IsEmptyOrNull($package.release) ? "null" : $package.release;
                $package.link = "/Update/" + $package.name + "/" + release + "/" + platform + "/" + $package.version;
                links.push($package);
            });
            return links;
        }
    }

    export class ISelfupdatePackage extends IFilePath {
        public version : string;
        public modified : number;
    }
}

namespace Com.Wui.Framework.Rest.Services.Utils {
    "use strict";
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import Loader = Com.Wui.Framework.Localhost.Loader;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Parent = Com.Wui.Framework.Hub.Utils.SelfupdatesManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ISelfupdatePackage = Com.Wui.Framework.Hub.Utils.ISelfupdatePackage;

    export class SelfupdatesManager extends Parent {
        // backwards compatibility API

        public static Download($appName : string, $releaseName : string = "", $platform : string = "", $version : string = null,
                               $callback? : any) : void {
            if (!ObjectValidator.IsString($releaseName)) {
                $callback = $releaseName;
                $releaseName = "";
                $platform = "";
                $version = "";
            } else if (!ObjectValidator.IsString($platform)) {
                $callback = $platform;
                $platform = "";
                $version = "";
            } else if (!ObjectValidator.IsString($version)) {
                $callback = $version;
                $version = "";
            }
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const filePath : ISelfupdatePackage =
                this.getInstance<SelfupdatesManager>().Download($appName, $releaseName, $platform, $version);
            if (!ObjectValidator.IsEmptyOrNull(filePath.path) && fileSystem.Exists(filePath.path)) {
                const fs : Types.NodeJS.fs = require("fs");
                const stats : any = fs.statSync(filePath.path);
                const fileSize : number = stats.size;
                const fileName : string = ObjectEncoder.Url(filePath.name);
                const headers : any = {
                    /* tslint:disable: object-literal-sort-keys */
                    "Pragma"                   : "public",
                    "Expires"                  : 0,
                    "Last-Modified"            : Convert.TimeToGMTformat(filePath.modified),
                    "Cache-Control"            : "private",
                    "Accept-Ranges"            : "bytes",
                    "Content-Encoding"         : "identity",
                    "Content-Transfer-Encoding": "binary",
                    "Content-Description"      : "File Transfer",
                    "Content-Length"           : fileSize,
                    "Content-Range"            : "0-" + (fileSize - 1) + "/" + fileSize,
                    "Content-Disposition"      : "attachment; filename=\"" + fileName + "\"",
                    "Content-Type"             : "application/octet-stream",
                    "Content-Version"          : filePath.version,
                    "x-content-version"        : filePath.version
                    /* tslint:enable */
                };
                $callback.owner.Send({
                    body     : require("fs").createReadStream(filePath.path),
                    headers,
                    normalize: false,
                    status   : HttpStatusType.SUCCESS
                });
            } else {
                $callback.owner.Send({
                    body  : "Unable to locate: " + (ObjectValidator.IsEmptyOrNull(filePath.path) ? filePath.url : filePath.path),
                    status: HttpStatusType.NOT_FOUND
                });
            }
        }

        protected getStoragePath() : string {
            const basePath : string = Loader.getInstance().getProgramArgs().AppDataPath() +
                "/resource/data/";
            let path : string = basePath + StringUtils.Replace(this.getClassName(), ".", "/");
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (!fileSystem.Exists(path)) {
                path = basePath + StringUtils.Replace(Parent.ClassName(), ".", "/");
            }
            this.getStoragePath = () : string => {
                return path;
            };
            return path;
        }
    }
}
