/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import FileSystemHandler = Com.Wui.Framework.Localhost.Connectors.FileSystemHandler;
    import ISendmailConfiguration = Com.Wui.Framework.Hub.Interfaces.ISendmailConfiguration;
    import ProgramArgs = Com.Wui.Framework.Hub.Structures.ProgramArgs;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class SendMail extends BaseConnector {

        @Extern()
        public static Send($to : string | string[], $subject : string, $body : string, $attachments : string[] = null,
                           $plainText : string = "", $copyTo : string | string[] = "", $from : string = "",
                           $callback? : (($status : boolean) => void) | IResponse) : void {
            if (ObjectValidator.IsArray($to)) {
                $to = (<string[]>$to).join(", ");
            }
            if (ObjectValidator.IsArray($copyTo)) {
                $copyTo = (<string[]>$copyTo).join(", ");
            }
            if (ObjectValidator.IsEmptyOrNull($from)) {
                $from = "WUI Report Service<report@services.wuiframework.com>";
            }

            if (ObjectValidator.IsEmptyOrNull($to)) {
                LogIt.Warning("Recipient must be specified.");
                ResponseFactory.getResponse($callback).Send(false);
            } else {
                const path : Types.NodeJS.path = require("path");
                const nodemailer : any = require("nodemailer");
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();

                $body = ObjectDecoder.Base64($body);
                if ((StringUtils.EndsWith($body, ".html") || StringUtils.EndsWith($body, ".txt")) &&
                    fileSystem.Exists($body)) {
                    $body = <string>fileSystem.Read($body);
                }
                $plainText = ObjectDecoder.Base64($plainText);
                if (!ObjectValidator.IsEmptyOrNull($body) && $body === StringUtils.StripTags($body)) {
                    $plainText = $body;
                    $body = "";
                }

                const programArgs : ProgramArgs = Loader.getInstance().getProgramArgs();
                const targetBase : string = programArgs.TargetBase();
                const storageBase : string = programArgs.AppDataPath();
                const attachments : any[] = [];
                if (!ObjectValidator.IsEmptyOrNull($attachments)) {
                    $attachments.forEach(($filePath : string) : void => {
                        let absolutePath : string = targetBase + $filePath;
                        let reValidate : boolean = false;
                        if (!fileSystem.Exists(absolutePath)) {
                            absolutePath = storageBase + $filePath;
                            reValidate = true;
                        }
                        if (reValidate && !fileSystem.Exists(absolutePath)) {
                            LogIt.Debug("Invalid attachment path \"{0}\".", absolutePath);
                        } else {
                            attachments.push({
                                filename: path.basename(absolutePath),
                                path    : absolutePath
                            });
                        }
                    });
                }
                if (!ObjectValidator.IsEmptyOrNull($body)) {
                    const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                    let index : number = 1;
                    const images : string[] = [];
                    const filter = /src=['"](.*?)['"]|url\(['"](.*?)['"]\)/gmi;
                    let sources : any = filter.exec($body);
                    while (sources != null) {
                        let path : string;
                        if (!ObjectValidator.IsEmptyOrNull(sources[1])) {
                            path = sources[1];
                        } else if (!ObjectValidator.IsEmptyOrNull(sources[2])) {
                            path = sources[2];
                        }
                        if (!ObjectValidator.IsEmptyOrNull(path)) {
                            if (images.indexOf(path)) {
                                images.push(path);
                            }
                        }
                        sources = filter.exec($body);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(images)) {
                        images.forEach(($src : string) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($src)) {
                                let absolutePath : string = targetBase + "/" + $src;
                                let reValidate : boolean = false;
                                if (!fileSystem.Exists(absolutePath)) {
                                    absolutePath = storageBase + "/" + $src;
                                    reValidate = true;
                                }
                                if (reValidate && !fileSystem.Exists(absolutePath)) {
                                    LogIt.Debug("Invalid image path \"{0}\".", absolutePath);
                                } else {
                                    let cid : string;
                                    if (index < 10) {
                                        cid = "00" + index;
                                    } else if (index < 100) {
                                        cid = "0" + index;
                                    } else {
                                        cid = "" + index;
                                    }
                                    cid = "image." + cid + ".src";
                                    index++;
                                    attachments.push({
                                        cid,
                                        filename: path.basename(absolutePath),
                                        path    : absolutePath
                                    });
                                    $body = StringUtils.Replace($body, "src=\"" + $src + "\"", "src=\"cid:" + cid + "\"");
                                    $body = StringUtils.Replace($body, "src='" + $src + "'", "src='cid:" + cid + "'");
                                    $body = StringUtils.Replace($body, "url(\"" + $src + "\")", "url(cid:" + cid + ")");
                                    $body = StringUtils.Replace($body, "url('" + $src + "')", "url(cid:" + cid + ")");
                                }
                            }
                        });
                    }
                }

                const logMessage : string = "Message with subject: \"{0}\" send to: \"{1}\" with status: {2}";
                const config : ISendmailConfiguration = Loader.getInstance().getAppConfiguration().sendmail;
                const transporter : any = nodemailer.createTransport({
                    auth  : {
                        pass: config.pass,
                        user: config.user
                    },
                    host  : config.location,
                    port  : config.port,
                    secure: true
                });

                transporter.sendMail(<any>{
                    /* tslint:disable: object-literal-sort-keys */
                    headers       : {
                        "X-Mailer": Loader.getInstance().getEnvironmentArgs().getProjectName() + "/" +
                            Loader.getInstance().getEnvironmentArgs().getProjectVersion(),
                        "X-Sender": $from
                    },
                    from          : $from,
                    to            : $to,
                    cc            : $copyTo,
                    subject       : $subject,
                    text          : $plainText,
                    html          : $body,
                    attachments,
                    attachDataUrls: true
                    /* tslint:enable */
                }, ($error : Error, $info : any) : void => {
                    if ($error) {
                        LogIt.Debug(logMessage, $subject, $to, "FAILURE");
                        if (!ObjectValidator.IsEmptyOrNull($error.message)) {
                            LogIt.Warning($error.message);
                        }
                        ResponseFactory.getResponse($callback).Send(false);
                    } else {
                        if (!ObjectValidator.IsEmptyOrNull($info.rejected)) {
                            LogIt.Warning("Some recipients has rejected mail: " + JSON.stringify($info.rejected));
                        }
                        LogIt.Debug(logMessage, $subject, $to, "SUCCESS");
                        ResponseFactory.getResponse($callback).Send(true);
                    }
                });
            }
        }
    }
}
