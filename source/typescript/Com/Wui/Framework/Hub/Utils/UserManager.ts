/* ********************************************************************************************************* *
 *
 * Copyright (c) 2010-2013 Jakub Cieslar
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import UserDao = Com.Wui.Framework.Hub.DAO.UserDao;
    import User = Com.Wui.Framework.Hub.Models.User;
    import BaseRegister = Com.Wui.Framework.Hub.Primitives.BaseRegister;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class UserManager extends BaseRegister<UsersTableItem> {
        private tokens : WebTokenManager;

        @Extern()
        public static LogIn($username : string, $password : string, $callback? : IResponse) : void {
            this.pipe<UserManager>($callback).LogIn($username, $password);
        }

        @Extern()
        public static LogOut($token : string, $callback? : IResponse) : void {
            this.pipe<UserManager>($callback).LogOut($token);
        }

        @Extern()
        public static IsAuthenticated($token : string, $callback? : IResponse) : void {
            this.pipe<UserManager>($callback).IsAuthenticated($token);
        }

        @Extern()
        public static IsAuthorizedFor($token : string, $requiredRoles : string | string[], $callback? : IResponse) : void {
            this.pipe<UserManager>($callback).IsAuthorizedFor($token, $requiredRoles);
        }

        @Extern()
        public static Register($username : string, $password : string, $callback? : IResponse) : void {
            this.pipe<UserManager>($callback).Register($username, $password);
        }

        @Extern()
        public static setRole($username : string, $role : string, $callback? : IResponse) : void {
            UserManager.pipe<UserManager>($callback).setRole($username, $role);
        }

        @Extern()
        public static Clear($username : string, $callback? : IResponse) : void {
            UserManager.getInstance<UserManager>().Clear($username);
            ResponseFactory.getResponse($callback).Send(LiveContentArgumentType.RETURN_VOID);
        }

        constructor() {
            super();
            this.tokens = new WebTokenManager();
        }

        public LogIn($username : string, $password : string) : string {
            const dao : UserDao = new UserDao(this.getUserId($username));
            if (!dao.Exists() || StringUtils.getSha1($username + $password) !== dao.getModel().Password()) {
                return "";
            } else {
                return this.tokens.Create(dao.getModel().Id());
            }
        }

        public LogOut($token : string) : boolean {
            return this.tokens.Clear($token);
        }

        public IsAuthenticated($token : string) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($token)) {
                const dao : UserDao = new UserDao(this.tokens.getPayloadId($token));
                return dao.Exists() && this.tokens.Validate($token);
            }
            return false;
        }

        public IsAuthorizedFor($token : string, $requiredRoles : string | string[]) : boolean {
            let status : boolean = false;
            let roles : string[] = [];
            if (ObjectValidator.IsString($requiredRoles)) {
                roles = StringUtils.Split(StringUtils.ToLowerCase(<string>$requiredRoles), " ", ",");
            } else {
                roles = <string[]>$requiredRoles;
            }
            if (!ObjectValidator.IsEmptyOrNull(roles)) {
                const dao : UserDao = new UserDao(this.tokens.getPayloadId($token));
                if (dao.Exists()) {
                    for (const role of roles) {
                        if (!ObjectValidator.IsEmptyOrNull(role) &&
                            StringUtils.ToLowerCase(dao.getModel().Role()) === StringUtils.Remove(role, " ")) {
                            status = true;
                            break;
                        }
                    }
                }
            } else {
                status = true;
            }
            return status;
        }

        public Register($username : string, $password : string) : boolean {
            const dao : UserDao = new UserDao(this.getUserId($username));
            if (!dao.Exists()) {
                const user : User = dao.getModel();
                user.Name($username);
                user.Password($password);
                if (dao.Save()) {
                    const register : UsersTableItem[] = this.getRegister();
                    let configExists : boolean = false;
                    register.forEach(($item : UsersTableItem) : void => {
                        if ($item.name === $username) {
                            $item.id = user.Id();
                            configExists = true;
                        }
                    });
                    if (!configExists) {
                        const newItem : UsersTableItem = new UsersTableItem();
                        newItem.name = $username;
                        newItem.id = user.Id();
                        register.push(newItem);
                    }
                    return Loader.getInstance().getFileSystemHandler().Write(this.getRegisterPath(), JSON.stringify(register));
                }
            }
            return false;
        }

        public setRole($username : string, $role : string) : boolean {
            const dao : UserDao = new UserDao(this.getUserId($username));
            if (dao.Exists()) {
                const user : User = dao.getModel();
                user.Role($role);
                return dao.Save();
            }
            return false;
        }

        public Clear($username : string) : void {
            const dao : UserDao = new UserDao(this.getUserId($username));
            dao.Delete();
        }

        private getUserId($username : string) : string {
            const register : UsersTableItem[] = this.getRegister();
            for (const item of register) {
                if (item.name === $username) {
                    return item.id;
                }
            }
            return this.getUID();
        }
    }

    export class UsersTableItem {
        public name : string;
        public id : string;
    }
}
