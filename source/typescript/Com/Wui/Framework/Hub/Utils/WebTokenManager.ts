/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Com.Wui.Framework.Gui.PersistenceFactory;
    import PersistenceType = Com.Wui.Framework.Gui.Enums.PersistenceType;
    import ITokenPayload = Com.Wui.Framework.Hub.Interfaces.ITokenPayload;
    import ITokenHeader = Com.Wui.Framework.Hub.Interfaces.ITokenHeader;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class WebTokenManager extends BaseObject {
        private secretExpirationTime : number = 20 * 60; // sec
        private authExpirationTime : number = 15 * 60; // sec
        private hashingAlgorithm : string = "sha512";
        private readonly payloads : ArrayList<ITokenPayload>;
        private persistence : IPersistenceHandler;

        constructor() {
            super();
            this.persistence = PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, this.getClassName());
            this.persistence.ExpireTime((this.secretExpirationTime + this.authExpirationTime) + " sec");
            if (this.persistence.Exists("payloads")) {
                this.payloads = this.persistence.Variable("payloads");
            } else {
                this.payloads = new ArrayList();
            }
        }

        public Create($id : string) : string {
            const header : ITokenHeader = <ITokenHeader>{
                alg : this.hashingAlgorithm,
                type: "jwt"
            };

            const payload : ITokenPayload = <ITokenPayload>{
                id: $id
            };

            $id = this.getPayloadId($id);
            if (this.payloads.KeyExists($id)) {
                this.payloads.RemoveAt(this.payloads.getKeys().indexOf($id));
            }
            return this.getToken(header, payload);
        }

        public Clear($token : string) : boolean {
            const id : string = this.getPayloadId($token);
            if (this.payloads.KeyExists(id)) {
                this.payloads.RemoveAt(this.payloads.getKeys().indexOf(id));
            }
            this.save();
            return this.payloads.KeyExists(id);
        }

        public Validate($token : string) : boolean {
            const tokenParts : string[] = StringUtils.Split($token, ".");
            if (tokenParts.length === 3) {
                const header : ITokenHeader = JSON.parse(ObjectDecoder.Base64(tokenParts[0]));

                const payload : ITokenPayload = this.payloads.getItem(ObjectDecoder.Base64(tokenParts[1]));

                if (ObjectValidator.IsObject(header) && !ObjectValidator.IsEmptyOrNull(payload)) {
                    if ($token === this.getToken(header, payload) && payload.expireTime - (new Date()).getTime() > 0) {
                        payload.expireTime = Property.Time(payload.expireTime, "+" + this.authExpirationTime + " sec");
                        LogIt.Debug("token is valid");
                        this.save();
                        return true;
                    } else {
                        LogIt.Debug("token is invalid");
                        this.Clear($token);
                    }
                } else {
                    LogIt.Debug("token header or payload is invalid {0} {1}", header, payload);
                }
            } else {
                LogIt.Debug("token format is invalid");
            }
            return false;
        }

        public getPayloadId($token : string) : string {
            const tokenParts : string[] = StringUtils.Split($token, ".");
            if (tokenParts.length === 3) {
                return ObjectDecoder.Base64(tokenParts[1]);
            }
            return null;
        }

        private getToken($header : ITokenHeader, $payload : ITokenPayload) : string {
            const crypto : any = require("crypto");
            const headerWithPayload : string = ObjectEncoder.Base64(JSON.stringify($header)) + "." + ObjectEncoder.Base64($payload.id);

            if (!this.payloads.KeyExists($payload.id)) {
                $payload.key = this.getUID();
                $payload.expireTime = Property.Time($payload.expireTime, "+" + this.authExpirationTime + " sec");
                this.payloads.Add($payload, $payload.id);
            }
            this.save();
            const hmac : any = crypto.createHmac($header.alg, this.payloads.getItem($payload.id).key);
            hmac.update(headerWithPayload);
            return headerWithPayload + "." + ObjectEncoder.Base64(hmac.digest("binary"));
        }

        private save() : void {
            this.persistence.Variable("payloads", this.payloads);
        }
    }
}
