/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import AgentsRegister = Com.Wui.Framework.Hub.Primitives.AgentsRegister;
    import IWuiAgent = Com.Wui.Framework.Hub.Primitives.IWuiAgent;

    export class WuiBuilderHub extends AgentsRegister {

        public static RunTask($task : IWuiBuilderTask, $callback? : IResponse) : void {
            let hash : string;
            let found : boolean = false;
            let message : string = "No agent is available.";
            for (hash in this.agents) {
                if (this.agents.hasOwnProperty(hash) && !found) {
                    const agent : IWuiAgent = this.agents[hash];
                    if (agent.owner === this) {
                        if ((ObjectValidator.IsEmptyOrNull($task.agent.name) ||
                            !ObjectValidator.IsEmptyOrNull($task.agent.name) && $task.agent.name === agent.name) &&
                            ($task.agent.version === "latest" || $task.agent.version === agent.version)) {
                            LogIt.Debug("Searching builders for platforms: {0}", $task.platforms);
                            $task.platforms.forEach(($platform : string) : boolean => {
                                let agentPlatform : string = "linux";
                                if (StringUtils.Contains($platform, "-win-", "-win32-")) {
                                    agentPlatform = "win";
                                } else if (StringUtils.Contains($platform, "-mac-")) {
                                    agentPlatform = "mac";
                                }

                                if (StringUtils.Contains(agent.platform, agentPlatform)) {
                                    LogIt.Info("Forward task \"" + JSON.stringify($task.args) + "\" " +
                                        "to builder [" + hash + "] with platform: " + agent.platform);
                                    const taskId : string = WuiBuilderHub.UID();
                                    this.tasks[taskId] = $callback;
                                    ResponseFactory.getResponse($callback).Send(<IWuiBuilderTaskProtocol>{
                                        builderId: hash,
                                        taskId
                                    });
                                    agent.connection.OnChange(<IWuiBuilderTaskProtocol>{
                                        data: $task.args,
                                        taskId,
                                        type: "taskRequest"
                                    });
                                    found = true;
                                } else {
                                    message = "No agent with suitable platform has been found.";
                                }
                                return !found;
                            });
                        } else {
                            message = "No agent with matching specified name \"" + $task.agent.name + "\" has been found.";
                        }
                    } else {
                        message = "No compatible agent found.";
                    }
                }
            }
            if (!found) {
                ResponseFactory.getResponse($callback).OnComplete(<IWuiBuilderTaskErrorProtocol>{
                    message,
                    status: false
                });
            }
        }
    }

    export abstract class IWuiBuilderTask {
        public args : string[];
        public platforms : string[];
        public agent : IWuiAgent;
    }

    export abstract class IWuiBuilderTaskProtocol {
        public builderId : string;
        public taskId : string;
        public data? : any;
        public type? : string;
    }

    export abstract class IWuiBuilderTaskErrorProtocol {
        public status : boolean;
        public message : string;
    }
}
