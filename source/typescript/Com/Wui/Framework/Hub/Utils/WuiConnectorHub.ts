/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub.Utils {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import AgentsRegister = Com.Wui.Framework.Hub.Primitives.AgentsRegister;
    import ILiveContentProtocol = Com.Wui.Framework.Services.Interfaces.ILiveContentProtocol;
    import IWuiAgent = Com.Wui.Framework.Hub.Primitives.IWuiAgent;

    export class WuiConnectorHub extends AgentsRegister {

        public static ForwardRequest($target : string, $data : ILiveContentProtocol, $callback? : IResponse) : void {
            let agent : IWuiAgent = null;
            let hash : string;
            for (hash in this.agents) {
                if (this.agents.hasOwnProperty(hash) && ObjectValidator.IsEmptyOrNull(agent)) {
                    if (this.agents[hash].owner === this && this.agents[hash].name === $target) {
                        agent = this.agents[hash];
                    }
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(agent)) {
                const taskId : string = WuiBuilderHub.UID();
                this.tasks[taskId] = $callback;
                agent.connection.OnChange(<IWuiConnectorRequestProtocol>{
                    protocol: $data,
                    taskId
                });
                ResponseFactory.getResponse($callback).Send(true);
            } else {
                ResponseFactory.getResponse($callback).Send(false);
            }
        }
    }

    export abstract class IWuiConnectorRequestProtocol {
        public protocol : ILiveContentProtocol;
        public taskId : string;
    }
}
