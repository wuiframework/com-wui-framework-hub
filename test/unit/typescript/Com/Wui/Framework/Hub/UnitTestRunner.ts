/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Hub {
    "use strict";
    import UnitTestEnvironmentArgs = Com.Wui.Framework.Localhost.UnitTestEnvironmentArgs;

    export class UnitTestLoader extends Loader {
        protected initEnvironment() : UnitTestEnvironmentArgs {
            return new UnitTestEnvironmentArgs();
        }
    }

    export class UnitTestRunner extends Com.Wui.Framework.UnitTestRunner {
        protected initLoader() : void {
            super.initLoader(UnitTestLoader);
        }
    }
}
